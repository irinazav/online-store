﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
//using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using DDD.Repositories.ContextStorage;

namespace DDD.Repositories.EF
{
    public class DataContextFactory
    {   
        public static DbContextEF GetDataContext()
        {
            IDataContextStorageContainer _dataContextStorageContainer = DataContextStorageFactory.CreateStorageContainer();

            DbContextEF storeDataContext = _dataContextStorageContainer.GetDataContext();
            if (storeDataContext == null)
            {
                storeDataContext = new DbContextEF();
                _dataContextStorageContainer.Store(storeDataContext);            
            }

            return storeDataContext;            
        }

        
    }
}
