﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace DDD.Repositories.EF
{
    public static  class InitDB
    {
        public static void Start()
        {
            Database.SetInitializer<DbContextEF>(new ContextInitializerEF());
        }
    }
}