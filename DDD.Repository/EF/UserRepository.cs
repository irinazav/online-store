﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DDD.Repositories.Abstract;
using DDD.Domain.Entities;
using DDD.Domain.User;
using DDD.Repositories.ContextStorage;
using DDD.Repositories.QueryModel;

namespace DDD.Repositories.EF
{
    public class UserRepository : IRepositoryCRUD<UserProfile, int>
    {
        private DbContextEF context;
        DbSet<UserProfile> set;

        public UserRepository()
        {
            context = DataContextFactory.GetDataContext();
            set = context.UserProfiles;
        }

        // DbContext
        public string GetEntitySetName()
        {
            return "Users";
        }

        public IQueryable<UserProfile> GetDBSet()
        {
            
            return set;
        }
       

        //Find All products
        public IQueryable<UserProfile> FindAll()
        {
            
            return GetDBSet();
        }

        public IQueryable<UserProfile> FindMatch(int id)
        {
            UserProfile user = GetDBSet().FirstOrDefault<UserProfile>(b => b.Id == id);

            return GetDBSet().Where(p => p.GenderId != user.GenderId);
        }


        public UserProfile FindBy(int id)
        {
            {
                return GetDBSet()
                    .Where(b => b.Id == id)
                    .Include("Anketa")
                    .FirstOrDefault();
            }
        }

        //Find products by query
       public ResponseByQuery FindBy(Query query)
        {
           
            return new ResponseByQuery();
        }

        public void Save(UserProfile entity)
        {
            UserProfile user = GetDBSet().FirstOrDefault<UserProfile>(b => b.Id == entity.Id);
            if (user != null)
            {
                context.Entry(user).CurrentValues.SetValues(entity);
                context.Entry(user).State = EntityState.Modified;
                context.SaveChanges();
            }
        }




        public void Remove(int id)
        {

           // context.Database.SqlQuery<Product>("DELETE * FROM ProductTitle where Id =1");

            System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@Id", 
                String.Format("it.{0}", id));
            context.Database.SqlQuery<ProductTitle>("Delete from ProductTitles WHERE Category_Id =  @Id", param);
            context.Database.SqlQuery<Product>("Delete from Products WHERE Category_Id =  @Id", param);

            UserProfile entity = GetDBSet().FirstOrDefault<UserProfile>(b => b.Id == id);
            context.Entry(entity).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void Add(UserProfile entity)
        {
            UserProfile c = new UserProfile() { Name = entity.Name };
            context.Entry(c).State = EntityState.Added;
            context.SaveChanges();
            entity.Id = c.Id;
        }

        public UserProfile GetImage(int id)
        {
            UserProfile user = context.UserProfiles
                               .Where(k => k.Id == id)
                               .FirstOrDefault<UserProfile>();
            if (user == null || user.ImageData == null) user = null;
            return user;

        }
        
    }
}


