﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using DDD.Domain.Entities;
using DDD.Domain.User;
using DDD.Repositories.ContextStorage;
using System.IO;
using DDD.Domain.Abstract;

namespace DDD.Repositories.EF
{
    public class ContextInitializerEF : DropCreateDatabaseAlways<DbContextEF>
    {

        private static void SetInitData<T>(string[] ar, DbContextEF db) where T : class, IAttribute, new()
        {

            for (int i = 0; i < ar.Length; i++)
            {
                T p = new T() { Id = i + 1, Name = ar[i] };
                db.Set<T>().Add(p);

            }
        }

        //----------------------------------------------------------
        private static void SetInitDataForState(InitStateNames[] ar, DbContextEF db)
        {

            for (int i = 0; i < ar.Length; i++)
            {
                State p = new State() { Id = i + 1, Name = ar[i].Name2 };
                db.Set<State>().Add(p);
            }
        }




        //---------------------------------------------------------
        protected override void Seed(DbContextEF db)
        {
            var a = true;
            if (a)
            {
                Brand pBrand1 = new Brand
                {
                    Id = 1,
                    Name = "Calvin Klein",
                    Description = @"Calvin Klein® is one of the leading fashion design and marketing studios in the world. It designs men's and women's designer collection clothing and a wide range of other products such as handbags, belts and shoes. Since its 1968 start, the Calvin Klein brand has become synonymous with sleek and sexy shoes, adorably functional handbags, and quality, stylish clothing.

Along with classic shoes, clothing and accessories, Calvin Klein's chic and understated style translates into the Calvin Klein Fragrance collection. Known the world over for such timeless scents as Obsession by Calvin Klein, Euphoria, CK One and Eternity for Men and Women, Calvin Klein fragrances bring a distinctive grace and modern elegance to the entire Calvin Klein collection. Calvin Klein's classic scents are also always evolving through the times and seasons to cater to the relentless pace of fashion. With a mixture of modern sophistication and classic sensibilities, Calvin Klein fragrances are synonymous with the brand's image of effortless and enduring style that defies trends and fads.

Being one of the most widely recognized fashion brands in the world, Calvin Klein is rapidly establishing the market position that a brand of its stature deserves. The successful performance of every new collection and the growing number of international stores is just the beginning of Calvin Klein's tremendous potential.

Keep an eye out for Calvin Klein clothing, Calvin Klein shoes, Calvin Klein handbags and Calvin Klein accessories as they climb to new heights in the fashion world."
                };




                Brand pBrand2 = new Brand
                {
                    Id = 2,
                    Name = "Jones New York",
                    Description = @"Jones New York® has been about dressing the modern American woman for over 35 years. The first suit, the first promotion, from graduation to the board room, Jones New York has been there helping her achieve her goals. We promise style that works for her in each of life's moments. "
                };




                Brand pBrand3 = new Brand
                {
                    Id = 3,
                    Name = "Via Spiga",
                    Description = @"Established in 1985, Via Spiga takes its name from a legendary street in Milan. The stunning collection of shoes, boots and sandals is steeped in a heritage of pure Italian designs, leathers and other materials. Via Spiga is a fashion brand and is seen regularly in top fashion magazines.

Via Spiga shoes are designed to be worn by every man and woman of style. The women's collection of pumps, mules, sandals, flats, wedges and boots cross over generations and demographics. The men's collection of shoes, boots and sandals is equally versatile and stylish. A Via Spiga design can be a trend shoe, a basic shoe, or an occasion shoe, but it’s always a beautiful shoe – bellissima!

Check out the high fashion designs of Via Spiga shoes. Known around the world for their playful cutting-edge shoes, sandals, and boots, the Via Spiga line continues to set fashion standards that others merely try to keep up with. Via Spiga leads the way to next year’s hottest footwear designs. "
                };





                Brand pBrand4 = new Brand
                {
                    Id = 4,
                    Name = "Dolche Vita",
                    Description = @"Launched in 2001, Dolce Vita's nonchalant attitude towards fashion resulted in footwear and clothing lines that are effortlessly stylish yet delightfully flirty. By showcasing chic looks attributed to the label’s very own modern muse—the young, the restless and the glamorous, Dolce Vita has developed an earnest following and is a favorite in great boutiques everywhere. From dresses to tops to sandals and heels, Dolce Vita is your one-stop-shop for everything posh.

Dolce Vita quickly became Southern California’s best kept secret. A favorite of the youthful and trend-conscious, its footwear and clothing showcase classic lines but with a flirty glint of fun. Once an exclusive presence of chic boutiques, Dolce Vita shoes and clothing are your choice to capture a little slice of West Coast ‘glam.’

The Dolce Vita girl exudes effortless cool. She takes the latest fashion trends and adds her own personal style in the mix, always a step ahead of the rest. Dolce Vita is always on the pulse of what’s going on in the pages of the hottest magazines and on the top must-have lists of fashion editors. With Dolce Vita shoes and clothing, you’ll always have the most sought-after styles in the hottest colors and silhouettes out there. "
                };



                Brand pBrand5 = new Brand
                {
                    Id = 5,
                    Name = "Patagonia",
                    Description = @"Traveling the globe in search of the best materials for their clothing and gear, Patagonia® leads the way with their innovative fabrics and technologies. Quality and performance are top priority. With every garment made there comes reassurance that it will not only be long lasting, but strong as well.

Patagonia's values reflect those of a business that started as a band of climbers and surfers who wanted to stay out as long as they could, doing what they love, no matter the weather. Their design approach to every product, from technical jackets for snow and ice to moisture-wicking underclothes and everyday clothes, demonstrates a functional bias toward simplicity and utility. Quality is impeccable, and the guarantee lifelong.

Their outerwear, which includes Patagonia sweaters, jackets, vests, fleece, and base layers, suits the needs of outdoor newcomers and enthusiasts alike.

An example of their top quality and research can be found with the Patagonia down products. They pride themselves as being the highest assurance of animal welfare in the apparel industry. All of the down in all of their down products is 100 percent “Traceable Down,” verified by an independent third-party expert who traced the down supply chain from farm to jacket—and every step in between.

A love for wild and beautiful places demands participation in the fight to save them and to help reverse the steep decline in the overall environmental health of the planet. As a brand, they use organic cotton; most polyester is recycled. They give moral support — and 1 percent of sales — to hundreds of grassroots environmental groups in the U.S., Europe, and Japan"
                };
                Brand pBrand6 = new Brand
                {
                    Id = 6,
                    Name = "Nic+Zoe",
                    Description = @"Dorian Lightbown, the Creative Director & designer behind NIC+ZOE, has always had a passion for knitwear. Even as a little girl, she didn’t just play with dolls—she styled and outfitted them. After attending Pratt and RISD as a young woman, she moved to Paris, which remains one of her favorite sources of design inspiration to this day. Then after over 20 years of bringing her ideas to life as a knitwear designer, she followed her entrepreneurial spirit and launched her own company in 2005.

Affectionately named after her two favorite people—her children, Nicholas & Zoë— the company is both a family business and a brand built upon feel-good fabrics. From finding the softest yarns in Shanghai to discovering new color palates in Italy, Dorian travels the world for the creative inspiration and technological innovations that help her seasonally reinvent knitwear. Her visionary approach to hand selecting textiles, designing prints, artisanally mixing colors and creating new stitching techniques ensures every design is unique. Each season, Dorian delivers a uniquely colored collection including sweaters, skirts, jackets, tops, dresses and pants, in both knits and wovens. It’s for women who wrap themselves in self-expression and wear their beauty from the inside, out—women like her daughter, Zoë, women like her industry mentors and, most of all, women like you. "
                };



                Brand pBrand7 = new Brand
                {
                    Id = 7,
                    Name = "BCBG",
                    Description = @"Founded in 1989, BCBGMAXAZRIA® is the creation of renowned fashion designer Max Azria. After years of success designing women’s wear in Paris and Los Angeles, Azria decided to pursue his dream of launching a design house that spoke to the modern woman. With the creation of the BCBGMAXAZRIA brand, he redefined the designer category by offering high-quality, on-trend clothing at contemporary price points.

BCBGMAXAZRIA began with a single idea—to create a beautiful dress. Today, they are one of America’s leading design houses, with a collection that includes evening and cocktail dresses, sportswear separates, handbags, footwear, and accessories. Designs are focused on fit, construction, and fabrication, and each piece represents the luxury, visionary viewpoint, and integrity of the brand itself.

Always on the forefront of fashion, the BCBGMAXAZRIA woman is known for her sense of style. She appreciates clothes, shoes, and accessories that are elegant as well as wearable. Her confidence is apparent in her attitude and flair for fashion. From clutches to sandals, each piece in the collection embodies the timeless spirit and chic sensibility of the woman who wears it.

BCBGMAXAZRIA knows that the one thing that makes a woman look her best is confidence. In BCBGMAXAZRIA clothes, shoes, and accessories, every woman feels like she can conquer the world with style.
"
                };


                Brand pBrand8 = new Brand { Id = 8, Name = "Ralph Lauren", Description = @"Lauren Ralph Lauren conveys a spirit of polished luxury each season through a stylish offering of shoes, handbags, swimwear, sleepwear, and jewelry. From workday to weekend, Lauren Ralph Lauren combines modernity with fresh, chic style." };



                Brand pBrand9 = new Brand
                {
                    Id = 9,
                    Name = "Miraclebody",
                    Description = @"Miraclebody® Jeans by Miraclesuit® - truly understands a woman's body.

The world leader in control swimwear has created a revolutionary new jean, which is designed and engineered for today's modern woman.

The unique and patent-pending 'secret panel' is designed to flatten and flatter the tummy, while the Miratex® fabric, a premium stretch denim, works to shape and slim the hips. Unlike other contour jeans that 'get baggy' after being worn for a few hours, Miraclebody jeans retain their shape all day and night long.

The continuous contoured waistband is slightly higher in the back than the front which assures there will be no unsightly views when sitting or bending. You know what we mean, don't you? You can actually pick up the baby out of the stroller and not have the 'coin slot' effect!

Miraclebody jeans have the perfect rise for maximum control with a modern fit. These jeans sit two fingers below the belly button - just enough for coverage and support, without looking frumpy.

Miraclebody Jeans by Miraclesuit LOVES YOUR CURVES!! You'll feel your best when you look your best! "
                };
                Brand pBrand10 = new Brand
                {
                    Id = 10,
                    Name = "The North Face",
                    Description = @"In 1966, two hiking enthusiasts founded a small mountaineering retail store in the heart of San Francisco's North Beach. The company soon became known as The North Face, a retailer of high-performance climbing and backpacking equipment. (The name was selected because in the Northern Hemisphere, the north face of a mountain is generally the coldest, iciest, and most formidable to climb.) In 1968, The North Face began designing and manufacturing its own brand of high-performance mountaineering apparel and equipment, and in the early 1980s, extreme skiwear was added to the product offering. By the end of the decade, TNF became the only supplier in the United States to offer a comprehensive collection of high-performance outerwear, skiwear, sleeping bags, packs, and tents.

Using all types of proprietary technology, TNF provides jackets and coats, pants, bags, and shoes with the quality, durability, and above all else, functionality that's required for outdoor adventure. Throughout the years, the jacket and outerwear division has set the standard in the industry. With styles such as the Denali Jacket and Vest, the Apex Bionic jackets, and the Nupste jackets, TNF proves time and time again that they are at the forefront of high-performance apparel.

Their dedication to footwear is no different. With the different categories of trail running, hiking, mountaineering, and most recently ultra running, their shoes division continues to raise the bar for innovation and quality in their shoes. Using proven technology such as Gore-Tex® and Gore-Tex® XCR®, their insulated footwear can sustain use in low, sub-zero temperatures, and with the innovative Boa® technology, users can have a precise fit with the ability to adjust quickly and on the fly. All features that make The North Face shoes a leader in the technical footwear arena.

The North Face’s dedication to quality can also be seen in their gear and equipment. Their sleeping bags are constructed with a high fill of goose down for warmth and multiple lengths. The daypacks and expedition packs, such as the Recon and Jester, are rugged, durable, and can hold all of your gear. The tents are built to withstand extreme temperatures on Everest and Mt. Rainier. If you have a desire to test the limits, The North Face equipment will help you on your adventure."
                };
                Brand pBrand11 = new Brand
                {
                    Id = 11,
                    Name = "Columbia",
                    Description = @"Who is Columbia®? They try things. That may sound like small potatoes. But more than talent, money, or good looks, trying is the fuel of progress. It’s the spirit that pushes people up mountains, down rivers, and across continents. Trying helped them put little silver dots on the inside of jackets, made shirts that repel bugs, and led to a rechargeable heating system in boots. If you think trying won’t help, remember the Grand Canyon started with one drop of water trying to get to the ocean. And Columbia was born as a small, family hat company in Portland. So try. Try often. Try differently. Try harder. Then try again. Greatness will follow.

Columbia. Trying stuff since 1938. "
                };
                Brand pBrand12 = new Brand
                {
                    Id = 12,
                    Name = "Kate Spade",
                    Description = @"Turn over a new leaf with Kate Spade New York® for 2015.

Crisp color, gorgeous graphic prints, and playful sophistication are the hallmarks of Kate Spade New York. From handbags, clothing, and swimwear to jewelry, footwear, tech accessories, tabletop, watches, and eyewear, the brand's exuberant approach to the everyday encourages personal style with a dash of incandescent charm.

Kate Spade New York has declared 2015 the year of adventure. Emerge from winter's fashion slumber to dazzle at garden parties! Run with a flamboyance of flamingos in their tropical habitat! Make a splash with the life aquatic! All this and so much more await you this year as you inform your look with the unmistakable class, elegance, beauty, sophistication, and playfulness that is Kate Spade New York."
                };






                Category pCategory1 = new Category { Id = 1, Name = "Coat" };
                Category pCategory2 = new Category { Id = 2, Name = "Dresses" };
                Category pCategory3 = new Category { Id = 3, Name = "Sweaters" };
                Category pCategory4 = new Category { Id = 4, Name = "Sleepwear" };
                Category pCategory5 = new Category { Id = 5, Name = "Pants" };

                //------------------------
                List<ProductQuantity> ppProductsQuantity1 = new List<ProductQuantity>();
                ppProductsQuantity1.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 9, Quantity = 15 });
                ppProductsQuantity1.Add(new ProductQuantity() { ProductSizeId = 6, ProductColorId = 9, Quantity = 5 });


                List<ProductQuantity> ppProductsQuantity2 = new List<ProductQuantity>();
                ppProductsQuantity2.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 4 });
                ppProductsQuantity2.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 7 });
                ppProductsQuantity2.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 8, Quantity = 4 });
                ppProductsQuantity2.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 8, Quantity = 4 });


                List<ProductQuantity> ppProductsQuantity3 = new List<ProductQuantity>();

                ppProductsQuantity3.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 1, Quantity = 45 });
                ppProductsQuantity3.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 1, Quantity = 45 });
                ppProductsQuantity3.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 45 });
                ppProductsQuantity3.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 8, Quantity = 45 });


                List<ProductQuantity> ppProductsQuantity4 = new List<ProductQuantity>();

                ppProductsQuantity4.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 1, Quantity = 145 });
                ppProductsQuantity4.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 2, Quantity = 425 });
                ppProductsQuantity4.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 1, Quantity = 25 });
                ppProductsQuantity4.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 2, Quantity = 825 });


                List<ProductQuantity> ppProductsQuantity5 = new List<ProductQuantity>();

                ppProductsQuantity5.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 1, Quantity = 145 });
                ppProductsQuantity5.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 5, Quantity = 425 });
                ppProductsQuantity5.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 25 });
                ppProductsQuantity5.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 8, Quantity = 825 });


                List<ProductQuantity> ppProductsQuantity6 = new List<ProductQuantity>();

                ppProductsQuantity6.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 5, Quantity = 145 });
                ppProductsQuantity6.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 7, Quantity = 425 });
                ppProductsQuantity6.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 8, Quantity = 25 });
                ppProductsQuantity6.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 6, Quantity = 825 });

                List<ProductQuantity> ppProductsQuantity7 = new List<ProductQuantity>();

                ppProductsQuantity7.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 5, Quantity = 145 });
                ppProductsQuantity7.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 425 });
                ppProductsQuantity7.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 25 });
                ppProductsQuantity7.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 8, Quantity = 825 });
                ppProductsQuantity7.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 8, Quantity = 825 });



                List<ProductQuantity> ppProductsQuantity8 = new List<ProductQuantity>();

                ppProductsQuantity8.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 1, Quantity = 145 });
                ppProductsQuantity8.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 5, Quantity = 145 });
                ppProductsQuantity8.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 5, Quantity = 145 });
                ppProductsQuantity8.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 425 });
                ppProductsQuantity8.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 8, Quantity = 25 });
                ppProductsQuantity8.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 5, Quantity = 825 });


                List<ProductQuantity> ppProductsQuantity9 = new List<ProductQuantity>();

                ppProductsQuantity9.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 145 });
                ppProductsQuantity9.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 425 });
                ppProductsQuantity9.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 5, Quantity = 25 });
                ppProductsQuantity9.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 5, Quantity = 825 });


                List<ProductQuantity> ppProductsQuantity10 = new List<ProductQuantity>();
                ppProductsQuantity10.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 1, Quantity = 145 });
                ppProductsQuantity10.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 145 });
                ppProductsQuantity10.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 145 });
                ppProductsQuantity10.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 8, Quantity = 425 });


                List<ProductQuantity> ppProductsQuantity11 = new List<ProductQuantity>();

                ppProductsQuantity11.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 145 });
                ppProductsQuantity11.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 8, Quantity = 425 });

                List<ProductQuantity> ppProductsQuantity12 = new List<ProductQuantity>();

                ppProductsQuantity12.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 7, Quantity = 145 });
                ppProductsQuantity12.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 7, Quantity = 145 });
                ppProductsQuantity12.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 8, Quantity = 425 });
                ppProductsQuantity12.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 8, Quantity = 425 });


                List<ProductQuantity> ppProductsQuantity13 = new List<ProductQuantity>();

                ppProductsQuantity13.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 3, Quantity = 5 });
                ppProductsQuantity13.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 3, Quantity = 5 });
                ppProductsQuantity13.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 5, Quantity = 5 });
                ppProductsQuantity13.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 5, Quantity = 5 });

                List<ProductQuantity> ppProductsQuantity14 = new List<ProductQuantity>();

                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 1, Quantity = 5 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 2, Quantity = 46 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 3, Quantity = 4 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 4, Quantity = 42 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 1, Quantity = 25 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 2, Quantity = 44 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 3, Quantity = 49 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 4, Quantity = 25 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 1, Quantity = 25 });
                ppProductsQuantity14.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 2, Quantity = 45 });


                List<ProductQuantity> ppProductsQuantity15 = new List<ProductQuantity>();

                ppProductsQuantity15.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 7, Quantity = 5 });
                ppProductsQuantity15.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 7, Quantity = 5 });
                ppProductsQuantity15.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 8, Quantity = 5 });

                List<ProductQuantity> ppProductsQuantity16 = new List<ProductQuantity>();

                ppProductsQuantity16.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 5, Quantity = 5 });
                ppProductsQuantity16.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 3, Quantity = 15 });

                List<ProductQuantity> ppProductsQuantity17 = new List<ProductQuantity>();

                ppProductsQuantity17.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 5, Quantity = 45 });
                ppProductsQuantity17.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 3, Quantity = 15 });
                ppProductsQuantity17.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 3, Quantity = 15 });
                ppProductsQuantity17.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 3, Quantity = 15 });


                List<ProductQuantity> ppProductsQuantity18 = new List<ProductQuantity>();

                ppProductsQuantity18.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 8, Quantity = 2 });
                ppProductsQuantity18.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 8, Quantity = 2 });
                ppProductsQuantity18.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 8, Quantity = 3 });
                ppProductsQuantity18.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 6, Quantity = 36 });
                ppProductsQuantity18.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 6, Quantity = 5 });
                ppProductsQuantity18.Add(new ProductQuantity() { ProductSizeId = 3, ProductColorId = 6, Quantity = 36 });

                List<ProductQuantity> ppProductsQuantity19 = new List<ProductQuantity>();

                ppProductsQuantity19.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 8, Quantity = 45 });
                ppProductsQuantity19.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 8, Quantity = 15 });
                ppProductsQuantity19.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 6, Quantity = 15 });
                ppProductsQuantity19.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 6, Quantity = 15 });


                List<ProductQuantity> ppProductsQuantity20 = new List<ProductQuantity>();

                ppProductsQuantity20.Add(new ProductQuantity() { ProductSizeId = 2, ProductColorId = 8, Quantity = 5 });
                ppProductsQuantity20.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 8, Quantity = 2 });
                ppProductsQuantity20.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 2, Quantity = 2 });
                ppProductsQuantity20.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 3, Quantity = 2 });
                ppProductsQuantity20.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 4, Quantity = 2 });




                List<ProductQuantity> ppProductsQuantity21 = new List<ProductQuantity>();

                ppProductsQuantity21.Add(new ProductQuantity() { ProductSizeId = 4, ProductColorId = 3, Quantity = 45 });
                ppProductsQuantity21.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 4, Quantity = 15 });


                List<ProductQuantity> ppProductsQuantity22 = new List<ProductQuantity>();

                ppProductsQuantity22.Add(new ProductQuantity() { ProductSizeId = 1, ProductColorId = 3, Quantity = 5 });
                ppProductsQuantity22.Add(new ProductQuantity() { ProductSizeId = 5, ProductColorId = 3, Quantity = 5 });

                //----------------------------------------------------------------------------------------------
                ProductImage pImage1 = new ProductImage()
                {
                    Id = 1
                };
                ProductImage pImage2 = new ProductImage()
                {
                    Id = 2
                };
                ProductImage pImage3 = new ProductImage()
                {
                    Id = 3
                };
                ProductImage pImage4 = new ProductImage()
                {
                    Id = 4
                };








                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/CatalogImages/", "");


                List<ProductTitleImage> pImages1 = new List<ProductTitleImage>();
                ProductTitleImage pImage;
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 }; //1
                pImage.SaveImageFromDisc(path + "p-1-1.jpg");
                pImages1.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-1-2.jpg");
                pImages1.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-1-3.jpg");
                pImages1.Add(pImage);


                // pImage1.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-1-1.jpg");
                db.Set<ProductTitleImage>().Add(pImage);


                ProductTitle pTitle1 = new ProductTitle
                {

                    Id = 1,
                    Name = "Rylee Sweater",
                    Price = 399.89M,
                    ProductsQuantity = ppProductsQuantity1,
                    Brand = pBrand12,
                    ProductTitleImages = pImages1,
                    Description = @"Crew neckline.
Three-quarter sleeves with ribbed cuffs.
Color-blocked.
Straight-cut ribbed hem.
60% cotton, 19% viscose, 16% polyamide, 5% cashmere.
Hand wash cold, reshape and dry flat.
Imported.",
                    Category = pCategory3
                };




                List<ProductTitleImage> pImages2 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 }; //1
                pImage.SaveImageFromDisc(path + "p-2-1.jpg");
                pImages2.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-2-2.jpg");
                pImages2.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-2-3.jpg");
                pImages2.Add(pImage);
                // pImage2.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-2-1.jpg");
                db.Set<ProductTitleImage>().Add(pImage);

                ProductTitle pTitle2 = new ProductTitle
                {
                    Id = 2,
                    Name = "Deco Rose Sweater",
                    Price = 303.89M,
                    ProductsQuantity = ppProductsQuantity2,
                    ProductTitleImages = pImages2,
                    Brand = pBrand12,
                    Description = @"Round neckline.
Long sleeves with ribbed cuffs.
Deco Rose pattern throughout.
Straight hemline.
51% polyamide, 46% mohair, 3% wool.
Hand wash cold, reshape and lay flat to dry.
Imported.",
                    Category = pCategory3


                };

                List<ProductTitleImage> pImages3 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-3-1.jpg");
                pImages3.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-3-2.jpg");
                pImages3.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-3-3.jpg");
                pImages3.Add(pImage);

                //  pImage3.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-3-1.jpg");

                ProductTitle pTitle3 = new ProductTitle
                {
                    Id = 3,
                    Name = "Twofer Sweater",
                    Price = 298.39M,
                    Brand = pBrand4,
                    ProductTitleImages = pImages3,
                    Description = @"Cotton-blend sweater in a classic stripe.
Structured V-panel neckline.
Dropped long sleeves.
Embroidered logo at left chest.
Slight high-low hemline.
Base layer not included.
55% cotton, 45% acrylic.
Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity3,
                    Category = pCategory3
                };





                List<ProductTitleImage> pImages4 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-4-1.jpg");
                pImages4.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-4-2.jpg");
                pImages4.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-4-3.jpg");
                pImages4.Add(pImage);
                //  pImage4.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-4-1.jpg");
                ProductTitle pTitle4 = new ProductTitle
                {
                    Id = 4,
                    Name = "Color Block  Sweater",
                    Price = 278.39M,
                    Brand = pBrand3,
                    //ProductImage = pImage4,
                    ProductTitleImages = pImages4,
                    ProductsQuantity = ppProductsQuantity4,
                    Description = @"Long-sleeve sweater in a linen and cotton knit.
Colorblocked striped design.
Ribbed crew neckline.
Ribbed cuffs.
Straight hem.
55% linen, 45% cotton.
Hand wash cold, dry flat.
Imported.",
                    Category = pCategory3,

                };




                // coat 

                List<ProductTitleImage> pImages5 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-5-1.jpg");
                pImages5.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-5-2.jpg");
                pImages5.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-5-3.jpg");
                pImages5.Add(pImage);

                ProductImage pImage5 = new ProductImage() { Id = 5 };
                // pImage5.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-5-1.jpg");
                ProductTitle pTitle5 = new ProductTitle
                {

                    Id = 5,
                    Name = "ThermoBall™ Parka",
                    Price = 279.99M,
                    Brand = pBrand10,
                    Category = pCategory1,
                    //ProductImage = pImage5,
                    ProductTitleImages = pImages5,
                    Description = @"Diamond quilting throughout the body.
Elastane binding at the hood opening and sleeve hem.
Exposed front zip with piping.
Internal waist adjustability.
Internal media pocket with media loop.
Embroidered logo on the left chest and back right shoulder.
bluesign® approved fabric.
Body: 100% nylon taffeta (bluesign® approved fabric).
Insulation: 100% polyester PrimaLoft® ThermoBall™.
Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity5
                };



                List<ProductTitleImage> pImages6 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-6-1.jpg");
                pImages6.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-6-2.jpg");
                pImages6.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-6-3.jpg");
                pImages6.Add(pImage);
                ProductImage pImage6 = new ProductImage() { Id = 6 };
                //pImage6.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-6-1.jpg");
                ProductTitle pTitle6 = new ProductTitle
                {

                    Id = 6,
                    Name = "Emma Jacket",
                    Price = 259.99M,
                    Brand = pBrand10,
                    Category = pCategory1,
                    //ProductImage = pImage6,
                    Description = @"Insulated, parka-length ski jacket keeps you warm and covered.
Water resistant and breathable exterior with a DWR coating.
Voluminous drape hood.
Two-snap, snap-down powder skirt with gripper elastic.
Front double-closure dickie construction.
Hand zip pockets.
Invisible wrist-accessory pocket with goggles cloth.
Internal Lycra® comfort cuffs.
Internal elasticated cuffs.
Internal goggle pocket.
Flattering ruched waist.
100% polyester novelty distressed mini herringbone shell with DWR.
550-fill down insulation (75% down, 25% feathers).
Machine wash cold, tumble dry low.
Imported.",
                    ProductTitleImages = pImages6,
                    ProductsQuantity = ppProductsQuantity6,
                };



                List<ProductTitleImage> pImages7 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-7-1.jpg");
                pImages7.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-7-2.jpg");
                pImages7.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-7-3.jpg");
                pImages7.Add(pImage);
                ProductImage pImage7 = new ProductImage() { Id = 7 };
                //pImage7.SaveImageFromDisc(path + "p-7-1.jpg");
                ProductTitle pTitle7 = new ProductTitle
                {

                    Id = 7,
                    Name = "Prow Jacket",
                    Price = 234.99M,
                    Brand = pBrand5,
                    Category = pCategory1,
                    // ProductImage = pImage7,
                    ProductTitleImages = pImages7,
                    Description = @"Motorcycle-style jacket with chevron quilt lines and asymmetrical front zipper.
Versatile, tall, snapped collar stands up or lies flat.
Zippered side hand pockets.
Long sleeves.
Hip length.
Shell: 1-oz 100% nylon plain weave with a DWR (durable water repellent) finish;
Insulation: 75% minimum down, 25% other fiber;
Lining: 100% nylon.
Machine wash warm, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity7,
                };




                List<ProductTitleImage> pImages8 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-8-1.jpg");
                pImages8.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-8-2.jpg");
                pImages8.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-8-3.jpg");
                pImages8.Add(pImage);
                ProductImage pImage8 = new ProductImage() { Id = 8 };
                //pImage8.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-8-1.jpg");
                ProductTitle pTitle8 = new ProductTitle
                {

                    Id = 8,
                    Name = "Lost Maples Jacket",
                    Price = 202.99M,
                    Brand = pBrand5,
                    Category = pCategory1,
                    //ProductImage = pImage8,
                    ProductTitleImages = pImages8,
                    Description = @"Regular fit is relaxed, but not sloppy, and perfect for everyday activities.
Polyester fleece motorcycle-style jacket with a faux-shearling collar.
Polartec® 10.5-oz.
Faux shearling at the collar, cuffs, and waist.
Oversized notched collar adjusts to stand up or fold down.
Long sleeves.
Off-center front zipper.
Hip length.
100% polyester.
Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity8,
                };



                List<ProductTitleImage> pImages9 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-9-1.jpg");
                pImages9.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-9-2.jpg");
                pImages9.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-9-3.jpg");
                pImages9.Add(pImage);
                ProductImage pImage9 = new ProductImage() { Id = 9 };
                //pImage9.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-9-1.jpg");
                ProductTitle pTitle9 = new ProductTitle
                {

                    Id = 9,
                    Name = "Leashless Jacket",
                    Price = 199.99M,
                    Brand = pBrand5,
                    Category = pCategory1,
                    //ProductImage = pImage9,
                    ProductTitleImages = pImages9,
                    Description = @"Microfleece-lined nape and wind flap for next-to-skin comfort.
Full front-zip closure.
Long sleeves feature hook-and-loop cuff closures.
Vented pit zips with coated, watertight, two-way zippers.
Exterior left chest pocket features a coated watertight zipper.
Interior right chest pocket.
Welted handwarmers with zippers treated with DWR (durable water repellent).

Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity9,
                };






                List<ProductTitleImage> pImages10 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-10-1.jpg");
                pImages10.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-10-2.jpg");
                pImages10.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-10-3.jpg");
                pImages10.Add(pImage);
                ProductImage pImage10 = new ProductImage() { Id = 10 };
                //pImage10.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-10-1.jpg");
                ProductTitle pTitle10 = new ProductTitle
                {

                    Id = 10,
                    Name = "Millennium Blur™ Jacket",
                    Price = 285.99M,
                    Brand = pBrand11,
                    Category = pCategory1,
                    //ProductImage = pImage10,
                    ProductTitleImages = pImages10,
                    Description = @"Underarm venting.
Waterproof zipper.
Drawcord adjustable hem.
Removable, snap back powder skirt.
Interior security pocket.
Media and goggle pocket.
Ski pass pocket.
Thumb holes.
Adjustable outer cuff-inner comfort cuff.
Articulated elbows.
Omni-Tech® shell: 86% nylon, 14% elastane King Stretch.
Omni-Heat® lining: 100% polyester thermal reflective microtex.
Omni-Heat® insulation: 60g 50% polyester, 50% recycled polyester.
Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity10,
                };




                //Dresses
                List<ProductTitleImage> pImages11 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-11-1.jpg");
                pImages11.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-11-2.jpg");
                pImages11.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-11-3.jpg");
                pImages11.Add(pImage);
                ProductImage pImage11 = new ProductImage() { Id = 11 };
                //pImage11.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-11-1.jpg");
                ProductTitle pTitle11 = new ProductTitle
                {

                    Id = 11,
                    Name = "Nadine Dress",
                    Price = 85.99M,
                    Brand = pBrand6,
                    Category = pCategory2,
                    //ProductImage = pImage11,
                    ProductTitleImages = pImages11,
                    Description = @"Sleeveless shirt dress flaunts a contemporary stripe print.
A-line silhouette.
Spread collar.
Button-flap chest pockets.
Elasticized waist.
Cutout detail at back.
Front button placket.
Dual button-flap detail at skirt.
68% cotton, 29% nylon, 3% elastane.
Machine wash cold, line dry.
Imported. ",
                    ProductsQuantity = ppProductsQuantity11,
                };





                List<ProductTitleImage> pImages12 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-12-1.jpg");
                pImages12.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-12-2.jpg");
                pImages12.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-12-3.jpg");
                pImages12.Add(pImage);
                ProductImage pImage12 = new ProductImage() { Id = 12 };
                //pImage12.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-12-1.jpg");
                ProductTitle pTitle12 = new ProductTitle
                {

                    Id = 12,
                    Name = "Rio Floral Dress",
                    Price = 125.99M,
                    Brand = pBrand7,
                    Category = pCategory2,
                    // ProductImage = pImage12,
                    ProductTitleImages = pImages12,
                    Description = @"V-neckline with open-knit mesh fabrication.
Sleeveless design.
High-contrast floral pattern with raised texture.
Mesh back yoke with keyhole opening.
Concealed zip closure at back.
Straight, mesh hem.
Partially lined.
Body: 74% nylon, 18% polyester, 8% spandex;
Combo: 100% polyester;
Lining: 100% polyester.
Hand wash cold, line dry.
Imported. ",
                    ProductsQuantity = ppProductsQuantity12,
                };





                List<ProductTitleImage> pImages13 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-13-1.jpg");
                pImages13.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-13-2.jpg");
                pImages13.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-13-3.jpg");
                pImages13.Add(pImage);
                ProductImage pImage13 = new ProductImage() { Id = 13 };
                //pImage13.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-13-1.jpg");
                ProductTitle pTitle13 = new ProductTitle
                {

                    Id = 13,
                    Name = "Color Block Crepe Dress",
                    Price = 105.99M,
                    Brand = pBrand7,
                    Category = pCategory2,
                    //ProductImage = pImage13,
                    ProductTitleImages = pImages13,
                    Description = @"Stretch jersey dress in a dramatic color block.
Subtle V-neckline with trim detail.
Cap-sleeve design.
Concealed zip closure at back.
Straight hem with rear vent.
Fully lined.
92% polyester, 8% spandex.
Lining: 100% polyester.
Professionally dry clean only.
Imported. ",
                    ProductsQuantity = ppProductsQuantity13,
                };





                List<ProductTitleImage> pImages14 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-14-1.jpg");
                pImages14.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-14-2.jpg");
                pImages14.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-14-3.jpg");
                pImages14.Add(pImage);
                ProductImage pImage14 = new ProductImage() { Id = 14 };
                //pImage14.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-14-1.jpg");
                ProductTitle pTitle14 = new ProductTitle
                {

                    Id = 14,
                    Name = "Sleeveless Dress",
                    Price = 99.99M,
                    Brand = pBrand7,
                    Category = pCategory2,
                    //ProductImage = pImage14,
                    ProductTitleImages = pImages14,
                    Description = @"Square neckline with V-shaped back.
Sleeveless construction.
Dual functional exposed zipper closure.
Straight hemline falls just above the knees.
61% viscose, 39% nylon.
Combo: 100% nylon.
Lining: 100% polyester.
Dry clean only.
Imported.",
                    ProductsQuantity = ppProductsQuantity14,
                };




                List<ProductTitleImage> pImages15 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-15-1.jpg");
                pImages15.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-15-2.jpg");
                pImages15.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-15-3.jpg");
                pImages15.Add(pImage);
                ProductImage pImage15 = new ProductImage() { Id = 15 };
                //pImage15.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-15-1.jpg");
                ProductTitle pTitle15 = new ProductTitle
                {

                    Id = 15,
                    Name = "Double Layer Tank Dress",
                    Price = 97.99M,
                    Brand = pBrand1,
                    Category = pCategory2,
                    //ProductImage = pImage15,
                    ProductTitleImages = pImages15,
                    Description = @"Curve-hugging dress features a contrast yoke for striking style.
A flirty ruffled overlay adds visual appeal.
Round neck and sleeveless construction.
Keyhole cutout at back with hook-and-eye closure.
Spit overlay at back.
Straight hem falls at mid-thigh.
Fully lined.
95% polyester, 5% spandex;
Contrast: 100% polyester;
Lining: 100% polyester.
Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity15,
                };




                // Sleepwear
                List<ProductTitleImage> pImages16 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-16-1.jpg");
                pImages16.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-16-2.jpg");
                pImages16.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-16-3.jpg");
                pImages16.Add(pImage);
                ProductImage pImage16 = new ProductImage() { Id = 16 };
                //pImage16.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-16-1.jpg");
                ProductTitle pTitle16 = new ProductTitle
                {

                    Id = 16,
                    Name = "Hartford PJ Set",
                    Price = 75.99M,
                    Brand = pBrand8,
                    Category = pCategory4,
                    //ProductImage = pImage16,
                    ProductTitleImages = pImages16,
                    Description = @"Pajama set is fabricated from a luxuriously soft cotton.
Long-sleeve top features a fold over collar and henley button placket.
Logo embroidery hits chest.
Contrast piping trims the collar, placket, and cuffs.
Long pajama pant has a drawstring waistband for an adjustable fit.
100% cotton.
Machine wash cold, tumble dry low.
Imported.",
                    ProductsQuantity = ppProductsQuantity16,
                };







                List<ProductTitleImage> pImages17 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-17-1.jpg");
                pImages17.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-17-2.jpg");
                pImages17.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-17-3.jpg");
                pImages17.Add(pImage);
                ProductImage pImage17 = new ProductImage() { Id = 17 };
                //pImage17.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-17-1.jpg");
                ProductTitle pTitle17 = new ProductTitle
                {

                    Id = 17,
                    Name = "Flannel PJ Set",
                    Price = 85.99M,
                    Brand = pBrand8,
                    Category = pCategory4,
                    //ProductImage = pImage17,
                    ProductTitleImages = pImages17,
                    Description = "Pajama set is fabricated from a super soft cotton.\nLong-sleeve top features a V-neckline with plaid trim.\nLong pajama pant has a drawstring waistband for an adjustable fit.\nTop: 60% cotton, 40% modal;\nBottom: 100% cotton.\nMachine wash cold, tumble dry low.\nImported.",
                    ProductsQuantity = ppProductsQuantity17,
                };



                //Pants
                List<ProductTitleImage> pImages18 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-18-1.jpg");
                pImages18.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-18-2.jpg");
                pImages18.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-18-3.jpg");
                pImages18.Add(pImage);
                ProductImage pImage18 = new ProductImage() { Id = 18 };
                //pImage18.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-18-1.jpg");
                ProductTitle pTitle18 = new ProductTitle
                {

                    Id = 18,
                    Name = "Nile Pattern Pant",
                    Price = 55.99M,
                    Brand = pBrand3,
                    Category = pCategory5,
                    //ProductImage = pImage18,
                    ProductTitleImages = pImages18,
                    Description = "Elasticized waistband with exterior drawstring tie for an adjustable fit.\nGeometric blocks add a bold and colorful pattern to this voluminous pant.\nSlash hand pockets provide easy access.\nWide leg.\n100% silk.\nDry clean only.\nImported.",
                    ProductsQuantity = ppProductsQuantity18,
                };







                List<ProductTitleImage> pImages19 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-19-1.jpg");
                pImages19.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-19-2.jpg");
                pImages19.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-19-3.jpg");
                pImages19.Add(pImage);
                ProductImage pImage19 = new ProductImage() { Id = 19 };
                //pImage19.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-19-1.jpg");
                ProductTitle pTitle19 = new ProductTitle
                {

                    Id = 19,
                    Name = "Katie Straight Leg",
                    Price = 65.99M,
                    Brand = pBrand9,
                    Category = pCategory5,
                    //ProductImage = pImage19,
                    ProductTitleImages = pImages19,
                    Description = "Patented hand-crafted canvas Miracle panel flattens the tummy for a trim silhouette.\nContoured waistband falls 1\" below the belly button.\nHigher back rise prevents embarrassing panty peek-a-boo.\nSuper stretch cotton-poly denim retains its shape for a polished appearance from sunup to sundown.\nFive-pocket design boasts signature back embroidery.\nPockets are stitched down on the inside to lay flat on the body.\nBelt loop waistband.\nZipper fly and button closure.\n88% cotton, 9% polyester, 3% spandex.\nMachine wash cold, tumble dry low. For best results, line dry.\nImported.",
                    ProductsQuantity = ppProductsQuantity19,
                };







                List<ProductTitleImage> pImages20 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 };
                pImage.SaveImageFromDisc(path + "p-20-1.jpg");
                pImages20.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-20-2.jpg");
                pImages20.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-20-3.jpg");
                pImages20.Add(pImage);
                ProductImage pImage20 = new ProductImage() { Id = 20 };
                //pImage20.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-20-1.jpg");
                ProductTitle pTitle20 = new ProductTitle
                {

                    Id = 20,
                    Name = "Secret Escape Pant",
                    Price = 45.99M,
                    Brand = pBrand7,
                    Category = pCategory5,
                    //ProductImage = pImage20,
                    ProductTitleImages = pImages20,
                    Description = "Crafted from a soft triblend.\nPant is fitted through the thigh and breaks above the knee to a bootcut leg opening.\nFlirty seam detail along back leg.\nFour-pocket design.\nBelt loops throughout waist.\nHidden button and hook at zip-fly closure.\n69% polyester, 29% viscose, 2% elastane;\nPocket lining: 100% polyester.\nDry clean only.\nMade in U.S.A. and Imported. ",
                    ProductsQuantity = ppProductsQuantity20,
                };




                ProductImage pImage21 = new ProductImage() { Id = 21 };
                List<ProductTitleImage> pImages21 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 }; //1
                pImage.SaveImageFromDisc(path + "p-21-1.jpg");
                pImages21.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-21-2.jpg");
                pImages21.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-21-3.jpg");
                pImages21.Add(pImage);
                //pImage21.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-21-1.jpg");
                db.Set<ProductTitleImage>().Add(pImage);


                ProductTitle pTitle21 = new ProductTitle
                {

                    Id = 21,
                    Name = "Crew Zip Sweater",
                    Price = 190.89M,
                    ProductsQuantity = ppProductsQuantity21,
                    Brand = pBrand1,
                    //ProductImage = pImage21,
                    ProductTitleImages = pImages21,
                    Description = "Crew neckline.\n" +
                                  "Long sleeve design.\n" +
                                  "Mock layered hemline.\n" +
                                  "60% cotton, 40% rayon.\n" +
                                  "Machine wash cold, reshape and dry flat.\n" +
                                  "Imported.",
                    Category = pCategory3
                };

                ProductImage pImage22 = new ProductImage() { Id = 22 };
                List<ProductTitleImage> pImages22 = new List<ProductTitleImage>();
                pImage = new ProductTitleImage() { Id = 1, ImageOrderId = 1 }; //1
                pImage.SaveImageFromDisc(path + "p-22-1.jpg");
                pImages22.Add(pImage);
                pImage = new ProductTitleImage() { Id = 2, ImageOrderId = 2 };
                pImage.SaveImageFromDisc(path + "p-22-2.jpg");
                pImages22.Add(pImage);
                pImage = new ProductTitleImage() { Id = 3, ImageOrderId = 3 };
                pImage.SaveImageFromDisc(path + "p-22-3.jpg");
                pImages22.Add(pImage);
                //pImage22.SaveImageFromDisc("C:\\Documents and Settings\\IRINA\\My Documents\\My Pictures\\DDD\\p-22-1.jpg");
                db.Set<ProductTitleImage>().Add(pImage);


                ProductTitle pTitle22 = new ProductTitle
                {

                    Id = 22,
                    Name = "Vintage Snow Sweater",
                    Price = 180.89M,
                    ProductsQuantity = ppProductsQuantity22,
                    Brand = pBrand3,
                    //ProductImage = pImage22,
                    ProductTitleImages = pImages22,
                    Description = "Boat neckline.\n" +
                                  "Long raglan sleeves\n" +
                                  "Button placket along front left shoulder.\n" +
                                  "Logo hit at left hip.\n" +
                                  "50% acrylic, 25% wool, 25% nylon.\n" +
                                  "Machine wash cold, reshape and dry flat\n" +
                                  "Imported.",
                    Category = pCategory3
                };



                db.ProductTitles.Add(pTitle1);
                db.ProductTitles.Add(pTitle2);
                db.ProductTitles.Add(pTitle3);
                db.ProductTitles.Add(pTitle4);
                db.ProductTitles.Add(pTitle5);
                db.ProductTitles.Add(pTitle6);
                db.ProductTitles.Add(pTitle7);
                db.ProductTitles.Add(pTitle8);
                db.ProductTitles.Add(pTitle9);
                db.ProductTitles.Add(pTitle10);
                db.ProductTitles.Add(pTitle11);
                db.ProductTitles.Add(pTitle12);
                db.ProductTitles.Add(pTitle13);
                db.ProductTitles.Add(pTitle14);
                db.ProductTitles.Add(pTitle15);
                db.ProductTitles.Add(pTitle16);
                db.ProductTitles.Add(pTitle17);
                db.ProductTitles.Add(pTitle18);
                db.ProductTitles.Add(pTitle19);
                db.ProductTitles.Add(pTitle20);
                db.ProductTitles.Add(pTitle21);
                db.ProductTitles.Add(pTitle22);



                SetInitData<Country>(Country.InitValue, db);
                SetInitDataForState(State.InitValue, db); // state!

                db.SaveChanges();
            }
        }
    }
}