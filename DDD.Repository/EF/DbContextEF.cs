﻿using System.Data.Entity;
//using System.Data.Objects;
using System.Data.Entity.Infrastructure;
using DDD.Domain.Entities;
using DDD.Domain.User;
using System.ComponentModel.DataAnnotations.Schema;

namespace DDD.Repositories.EF
{
    public class DbContextEF : DbContext
    {
        // init DB 1-st time
        static DbContextEF()
        {
           Database.SetInitializer<DbContextEF>(new ContextInitializerEF());
        }

        public DbSet<ProductTitle> ProductTitles { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductQuantity> ProductsQuantity { get; set; }
        public DbSet<ProductTitleImage> ProductTitleImages { get; set; }

        public DbSet<State> States { get; set; }
        public DbSet<Country> Countries { get; set; }

        // User
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Anketa> Anketas { get; set; }
       

        // TPC -> Table Per Concrete Type
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
            .Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Categories");
            });

            modelBuilder.Entity<Brand>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Brands");
            });


            modelBuilder.Entity<ProductTitle>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("ProductTitles");
            });

          

            modelBuilder.Entity<ProductImage>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Images");
            });


            modelBuilder.Entity<State>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("States");
            });

            modelBuilder.Entity<Country>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Countries");
            });

            modelBuilder.Entity<ProductQuantity>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("ProductsQuantity");
            });

            modelBuilder.Entity<ProductTitleImage>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("ProductTitleImages");
            });

            modelBuilder.Entity<UserProfile>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("UserProfiles");
            });

            modelBuilder.Entity<Anketa>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Anketas");
            });

            

            modelBuilder.Entity<Brand>()
                   .Property(e => e.Id)
                   .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Category>()
                  .Property(e => e.Id)
                  .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<ProductImage>()
                  .Property(e => e.Id)
                  .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Product>()
                  .Property(e => e.Id)
                  .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<ProductQuantity>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ProductTitle>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ProductTitleImage>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<State>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Country>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


         modelBuilder.Entity<UserProfile>()
                 .Property(e => e.Id)
                 .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Anketa>()
                .Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);}
        }
    }
