﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DDD.Repositories.Abstract;
using DDD.Domain.Entities;
using DDD.Repositories.ContextStorage;
using DDD.Repositories.QueryModel;

namespace DDD.Repositories.EF
{
    public class CategoryRepository : IRepositoryCRUD<Category, int>
    {
        private DbContextEF context;
        DbSet<Category> set;

        public ResponseByQuery FindBy(Query query)
        {

            return new ResponseByQuery();
        }

        public CategoryRepository()
        {
            context = DataContextFactory.GetDataContext();
            set = context.Categories;
        }

        // DbContext
        public string GetEntitySetName()
        {
            return "Categories";
        }

        public IQueryable<Category> GetDBSet()
        {
            
            return set;
        }
       

        //Find All products
        public IQueryable<Category> FindAll()
        {
            return GetDBSet();
        }

        
        public Category FindBy(int id)
        {
            {
                return GetDBSet().FirstOrDefault<Category>(b => b.Id == id);
            }
        }

        


        public void Save(Category entity)
        {
            Category category = GetDBSet().FirstOrDefault<Category>(b => b.Id == entity.Id);
            if (category != null)
            {
                category.Name = entity.Name;
                context.Entry(category).State = EntityState.Modified;
                context.SaveChanges();
            }
        }




        public void Remove(int id)
        {

            System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@Id", 
            String.Format("it.{0}", id));
            context.Database.SqlQuery<ProductTitle>("Delete from ProductTitles WHERE Category_Id =  @Id", param);
            
            
            Category entity =  GetDBSet().FirstOrDefault<Category>(b => b.Id == id);
            context.Entry(entity).State = EntityState.Deleted;
            context.SaveChanges();
        }

        public void Add(Category entity)
        {
            Category c = new Category() { Name = entity.Name };
            context.Entry(c).State = EntityState.Added;
            context.SaveChanges();
            entity.Id = c.Id;
        }

        public int CountByProduct(int id)
        {
            return context.ProductTitles.Where(p => p.Brand.Id == id).Count();

        }
        
    }
}

