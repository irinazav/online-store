﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Repositories.EF;

namespace DDD.Repositories.ContextStorage
{
    public interface IDataContextStorageContainer
    {
        DbContextEF GetDataContext();
        void Store(DbContextEF storeDataContext);
       
    }
}
