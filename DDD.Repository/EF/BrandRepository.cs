﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DDD.Repositories.Abstract;
using DDD.Domain.Entities;
using DDD.Repositories.ContextStorage;
using DDD.Repositories.QueryModel;

namespace DDD.Repositories.EF
{
    public class BrandRepository : IRepositoryCRUD<Brand, int>
    {
        private DbContextEF context;
        DbSet<Brand> set;

        public ResponseByQuery FindBy(Query query)
        {

            return new ResponseByQuery();
        }

        public BrandRepository()
        {
            context = DataContextFactory.GetDataContext();
            set = context.Brands;
        }

      
        public string GetEntitySetName()
        {
            return " Brands";
        }

        public IQueryable<Brand> GetDBSet()
        {

            return set;
        }


       
        public IQueryable<Brand> FindAll()
        {
            return GetDBSet();
        }


        public Brand FindBy(int id)
        {
            {
                return context.Brands.Find(id);
            }
        }


        public void Save(Brand entity)
        {
            Brand brand = GetDBSet().FirstOrDefault<Brand>(b => b.Id == entity.Id);
            if (brand != null)
            {
                brand.Name = entity.Name;
                brand.Description = entity.Description;
                context.Entry(brand).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public int CountByProduct(int id) 
        {
            return context.ProductTitles.Where(p => p.Brand.Id == id).Count();

        }


        public void Remove(int id)
        {
            System.Data.SqlClient.SqlParameter param = new System.Data.SqlClient.SqlParameter("@Id",
                        String.Format("it.{0}", id));
            context.Database.SqlQuery<ProductTitle>("Delete from ProductTitles WHERE Brand_Id =  @Id", param);
             Brand entity = GetDBSet().FirstOrDefault<Brand>(b => b.Id == id);
             context.Entry(entity).State = EntityState.Deleted;
             context.SaveChanges();
            
        }

        public void Add(Brand entity)
        {
            Brand c = context.Brands.Add(entity);         
            context.SaveChanges();
           
        }



    }
}

