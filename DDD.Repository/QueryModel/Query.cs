﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Entities;

namespace DDD.Repositories.QueryModel
{
    public  class Query
    {
        public int CategoryId { get; set; }

        public int[] ColorIds { get; set; }
        public int[] BrandIds { get; set; }
        public int[] SizeIds { get; set; }

        public OrderByClause OrderByProperty { get; set; }

       

    }
}
