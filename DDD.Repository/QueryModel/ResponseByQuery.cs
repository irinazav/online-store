﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Entities;

namespace DDD.Repositories.QueryModel
{
    public class ResponseByQuery
    {

        public IEnumerable<ProductTitle> Products { get; set; }
        public IEnumerable<Brand> MatchBrands { get; set; }

        public int[] MatchColors { get; set; }
        public int[] MatchSizes { get; set; }
   


    }
}
