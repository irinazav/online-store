﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using DDD.Domain.Abstract;
using DDD.Repositories.EF;
using DDD.Repositories.QueryModel;
namespace DDD.Repositories.Abstract
{
    // CRUD -> *R**
    public interface IRepositoryR<T, TId> where T: class, IAggregateRoot  
    {


        IQueryable<T> GetDBSet();

        string GetEntitySetName();
        T FindBy(TId id);
        IQueryable<T> FindAll();
        ResponseByQuery FindBy(Query query);
        //IQueryable<T> FindBy(Query query, int index, int count);

        
         
        
    }
    
}
