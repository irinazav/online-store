﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;

namespace DDD.Repositories.Abstract
{
    public interface IRepositoryCRUD<T, TId> : IRepositoryCUD<T, TId>, IRepositoryR<T, TId>
                                                     where T : class, IAggregateRoot
    {
    }
}
