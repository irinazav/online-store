﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Entities;
using DDD.WebUI.ViewModels;

namespace DDD.WebUI.ModelBinder
{
    public class ProductTitleModelBinder: IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
                                    ModelBindingContext modelBindingContext)
        {
           

          /*  ProductEditAdminViewModel model = new ProductEditAdminViewModel();
           // model.ColorIds
            int[] str = modelBindingContext.ValueProvider.GetValue("ColorIds")
                                                   .AttemptedValue.Split(',')
                                                   .Select(p => int.Parse(p));

            */
            return new ProductTitle();
        }

        
    }
}