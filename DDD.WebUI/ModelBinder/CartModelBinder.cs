﻿using System;
using System.Web.Mvc;
using DDD.Domain.Basket;

namespace DDD.WebUI.ModelBinder
{
    // add to global.asax.cs
    //ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());

    public class CartModelBinder : IModelBinder
    {
        private const string sessionKey = "Cart";
        public object BindModel(ControllerContext controllerContext, ModelBindingContext
                bindingContext)
        {
            
            Cart cart = (Cart)controllerContext.HttpContext.Session[sessionKey];

            
            if (cart == null)
            {
                cart = new Cart();
                controllerContext.HttpContext.Session[sessionKey] = cart;
            }
            
            return cart;
        }
    }
}