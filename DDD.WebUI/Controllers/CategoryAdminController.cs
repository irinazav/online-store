﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Entities;
using DDD.Service.Admin;

namespace DDD.WebUI.Controllers
{
    public class CategoryAdminController : Controller
    {
        private IAdminServices<Category, int> adminServices;


        public CategoryAdminController(IAdminServices<Category, int> adminServices)
        {
            this.adminServices = adminServices;
        }  

       

        public ActionResult Index()
        {
            
            return View(adminServices.GetAll());
            
        }



        public ViewResult Create()
        {
            return View("Edit", new Category() { Id = 0 });
        }
  

        public ActionResult Edit(int Id)
        {
            return View(adminServices.GetBy(Id));
        }

       
        [HttpPost]
        public ActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                if (category.Id == 0) adminServices.Add(category);
                else adminServices.Save(category);
                         return RedirectToAction("Index");
            }
            else
            {
                return View(category);
            }
        }


        [HttpPost]
        public ActionResult Delete(int Id)
        {
            Category category = adminServices.GetBy(Id); 
            if (category != null)
            {
            
                 adminServices.Remove(Id);
            }
            return RedirectToAction("Index");
        }

        
    }
}
