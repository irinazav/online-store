﻿
 using System.Collections.Generic;
using System.Web.Mvc;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Service.Model;
using DDD.Service.Product;
using DDD.WebUI.ViewModels.Product;
using DDD.WebUI.Infrastructure;
using DDD.Domain.Basket;

namespace DDD.WebUI.Controllers
{
    public class ProductCatalogBaseController :Controller
    {
       private readonly IProductCatalogSevices _productCatalogService;
      

        //--------------------------------------------------
        public ProductCatalogBaseController(
                          
                          IProductCatalogSevices productCatalogService)
           
        {
            _productCatalogService = productCatalogService;
            
        }


        //--------------------------------------------------
        public IEnumerable<Category> GetCategories()
        {
            IEnumerable<Category> response =
                                _productCatalogService.GetAllCategories();

            return response;
        }


        
    }

}