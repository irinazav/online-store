﻿using DDD.Domain.Entities;
using DDD.Domain.Abstract;
using DDD.Domain.Basket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Repositories.Abstract;
using DDD.WebUI.ViewModels.Basket;
using DDD.WebUI.ModelBinder;
using DDD.Domain.Shipping;
using DDD.Domain.Order;
using DDD.Repositories.EF;

namespace DDD.WebUI.Controllers
{
    public class CartController : Controller
    {
        private IRepositoryR<ProductTitle, int> repository;
        private IOrderProcessor orderProcessor;



        //--------------------------------------------------
        public CartController(IRepositoryR<ProductTitle, int> repo, IOrderProcessor proc)
        {
            repository = repo;
            orderProcessor = proc;
        }
       
        //----------------------------------------------------
        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(cart);
        }

       //------------------------------------------------------
        public ViewResult Index(Cart cart, string returnUrl)
        {


            return View(new CartIndexViewModel
            {

                Cart = cart,
                ReturnUrl = returnUrl
            });
        }

      
        //----------------------------------------------
        public RedirectToRouteResult AddToCart(Cart cart, string returnUrl,
            int productId , int ProductColorId, int ProductSizeId,  int ProductQuantity)
        {

            ProductTitle product = repository.FindBy(productId);
            if (product != null)
            {

                cart.AddItem(product, ProductColorId,  ProductSizeId,  ProductQuantity);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        
       
        //-------------------------------------------------------------------
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult AddToCartAjax(Cart cart,
           int ProductId, int ProductColorId, int ProductSizeId, int ProductQuantity)
        {
          
            ProductTitle product = repository.FindBy(ProductId);
            if (product != null)
            {              
                cart.AddItem(product, ProductColorId, ProductSizeId, ProductQuantity);
            }
            var newdata = new
            {
                CartQuantity = cart.TotalItems(),
                CartTotal = cart.ComputeTotalValue()
            };
            return Json(newdata, JsonRequestBehavior.AllowGet);
        }


        //--------------------------------------------
        public RedirectToRouteResult RemoveFromCart(Cart cart, int productId, string returnUrl)
        {
            ProductTitle product = repository.FindBy(productId);
            if (product != null)
            {
               
                cart.RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        //---------------------------------------
        public ActionResult Checkout()
        {
            ShippingDetails details = new ShippingDetails();
            details.States = ((ProductTitleRepository)repository).GetProductStates();
            return View(details);
        }



        //-----------------------------------------
        [HttpPost]
        public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
        {
            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, your cart is empty!");
            }

          
            if (shippingDetails.StateId == 0)
            {
                ModelState.AddModelError("StateId", "Please select a state");
                
            }

            if (ModelState.IsValid)
            {
                //orderProcessor.ProcessOrder(cart, shippingDetails);
                ((ProductTitleRepository)repository).ProcessOrder(cart.Lines);

                cart.Clear();
                return View("Completed");
            }
            else
            {

                    ShippingDetails details = new ShippingDetails();
                    shippingDetails.States = ((ProductTitleRepository)repository).GetProductStates();

                return View(shippingDetails);




            }
        }
    }
}
