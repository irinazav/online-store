﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.WebUI.ViewModels.Admin;

namespace DDD.WebUI.Controllers
{
    public class AdminController : Controller
    {
        public ActionResult GetCategoryCatalog()
        {
            return View("CategoryCatalog");
        }
        public ActionResult Index()
        {
            AdminMenu menu = new AdminMenu();
            return View(menu);
        }
      

    }
}
