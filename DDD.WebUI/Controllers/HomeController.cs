﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Service.Model;
using DDD.Service.Product;
using DDD.Domain.Basket;
using DDD.WebUI.ViewModels.Product;
using DDD.WebUI.Infrastructure;


namespace DDD.WebUI.Controllers
{
    public class HomeController : ProductCatalogBaseController
    {
     private readonly IProductCatalogSevices _productCatalogService;
   

       public HomeController(Cart cart, IProductCatalogSevices productCatalogService)
            : base( productCatalogService)
        {
            _productCatalogService = productCatalogService;
            
        }
 
        public ActionResult Index(Cart cart, int ItemsPerPage = 8, int CurrentPage = 1,
             int SortBy = 1)
        {
            IEnumerable<ProductTitle> products = _productCatalogService.GetFeaturedProducts();
            int numberOfTitlesFound = products.Count();
            int totalNumberOfPages = (int)Math.Ceiling((decimal)numberOfTitlesFound / ItemsPerPage);

            

            PagingInfoModel pagingInfoModel = new PagingInfoModel()
            {
                CurrentPage = CurrentPage,
                NumberOfTitlesFound = numberOfTitlesFound,
                TotalNumberOfPages = totalNumberOfPages
            };
          
            HomePageViewModel homePageViewModel = new HomePageViewModel();
            homePageViewModel.Categories = base.GetCategories();
            homePageViewModel.Cart = cart;
            homePageViewModel.PagingInfoModel = pagingInfoModel;
            homePageViewModel.ItemsPerPage = ItemsPerPage;
            homePageViewModel.SortBy = SortBy;

            ProductsSortBy sortBy = (ProductsSortBy)SortBy;
            switch (sortBy)
            {
                case ProductsSortBy.PriceLowToHigh:
                   products = products.OrderBy(p => p.Price);
                    break;
                case ProductsSortBy.PriceHighToLow:
                    products = products.OrderByDescending(p => p.Price);
                    break;
            }

            homePageViewModel.Products = products
                              .Skip((CurrentPage - 1) * ItemsPerPage).Take(ItemsPerPage);
                                 
            return View(homePageViewModel);
        }
    }
}
