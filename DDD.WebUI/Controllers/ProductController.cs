﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Service.Product;
using DDD.Service.Model;
using DDD.WebUI.ViewModels.Product;
using DDD.WebUI.JsonDTOs;
using DDD.Domain.Basket;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;


namespace DDD.WebUI.Controllers
{
    public class ProductController : ProductCatalogBaseController
    {

       private IProductCatalogSevices service;
       

//----------------------------------------------------------------------
        public ProductController(Cart cart, IProductCatalogSevices productService)
            : base( productService)
        {
            this.service = productService;
        }

        //----------------------------------------------------------
        public ActionResult GetProductsByCategory(int categoryId,  Cart cart, int itemsPerPage= 8)
        {
            ProductsByCategoryRequestModel productSearchRequest =
                        GenerateInitialProductSearchRequestFrom(categoryId, itemsPerPage);

            ProductsByCategoryResponseModel response =
                        service.GetProductsByCategory(productSearchRequest);

            ProductSearchResultViewModel productSearchResultView =
                        GetProductSearchResultViewFrom(response);
            productSearchResultView.ItemsPerPage = itemsPerPage;
            productSearchResultView.Cart = cart;

            return View("ProductSearchResult", productSearchResultView);
        }


        // New Search By Category with dafault setting
        //--------------------------------------------------------
        private static ProductsByCategoryRequestModel
                               GenerateInitialProductSearchRequestFrom(int categoryId, int itemsPerPage)
        {
            ProductsByCategoryRequestModel productSearchRequest =
                                     new ProductsByCategoryRequestModel();
            productSearchRequest.NumberOfResultsPerPage = itemsPerPage;
            productSearchRequest.CategoryId = categoryId;
            productSearchRequest.Index = 1;
            productSearchRequest.SortBy = ProductsSortBy.PriceHighToLow;
            return productSearchRequest;
        }
     
        //-----------------------------------------------------------
        private ProductSearchResultViewModel GetProductSearchResultViewFrom(
                                      ProductsByCategoryResponseModel response)
        {
            ProductSearchResultViewModel productSearchResultView =
                                              new ProductSearchResultViewModel();
           productSearchResultView.Categories = base.GetCategories();
           productSearchResultView.Products = response.Products.ToList();
           productSearchResultView.RefinementGroups = response.RefinementGroups;
           productSearchResultView.SelectedCategory = response.SelectedCategory;
           productSearchResultView.SelectedCategoryName = response.SelectedCategoryName;

           productSearchResultView.PagingInfoModel = new PagingInfoModel
           {
               TotalNumberOfPages = response.TotalNumberOfPages,
               NumberOfTitlesFound = response.NumberOfTitlesFound,
               CurrentPage = response.CurrentPage
           }; 

          return productSearchResultView;
        }

        
        public ActionResult GetCategoriesMenu ()
        {

            return PartialView("_Categories", 
                new BaseProductCatalogPageViewModel() {  Categories = base.GetCategories()});
        }

            //----------------------------------------------------
        public JsonResult GetProductQuantity(int SearchId, int ProductId, int ColorId)
        {
            ProductTitle p = service.GetProduct(ProductId);

            var newData = p.ProductsQuantity
                    .Where(c => c.ProductColorId == ColorId && SearchId == c.ProductSizeId)
                    .FirstOrDefault();
            var quantity = 0;
            if (newData != null) quantity = newData.Quantity;

            return Json(new { Quantity = quantity }, JsonRequestBehavior.AllowGet);
        }
        
        //----------------------------------------------------
        public JsonResult GetSizesForColor(int SearchId, int ProductId)
        {
            ProductTitle p = service.GetProduct(ProductId);

            var newData = p.ProductsQuantity
                    .Where( c => c.ProductColorId ==  SearchId)
                    .Select(c => c.ProductSizeId)
                    .Distinct().OrderBy( g => g)
                    .Select( g => new { Id = g, Name = ((ProductSize)g).ToString()});
            return Json(newData, JsonRequestBehavior.AllowGet);
        }
        
        //------------------------------------------------------
       [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetProductsByAjax( JsonProductSearchRequest jsonProductSearchRequest)
        {    
           
            ProductsByCategoryRequestModel productSearchRequest =
                          GenerateProductSearchRequestFrom(jsonProductSearchRequest);
            ProductsByCategoryResponseModel response =
                         service.GetProductsByCategory(productSearchRequest);
            ProductSearchResultViewModel productSearchResultView =
                         GetProductSearchResultViewFrom(response);

            var newData = new
            {
                RefinementGroups = productSearchResultView.RefinementGroups,
                SelectedCategory = productSearchResultView.SelectedCategory,
                SelectedCategoryName = productSearchResultView.SelectedCategoryName,
                PagingInfoModel = productSearchResultView.PagingInfoModel,
                Products = productSearchResultView.Products
                   .Select(p => new
                   {
                       Id = p.Id,
                       Name = p.Name,
                       Price = p.Price,

                       ProductColors = p.ProductsQuantity.Select(c => new
                       {
                           ColorName = c.ProductColorValue.ToString(),
                           ColorId = c.ProductColorId.ToString()
                       }).Distinct().ToArray(),      
                      
                       BrandName = p.Brand.Name
                   })
            };

            return Json(newData, JsonRequestBehavior.AllowGet);
        }


        //--------------------------------------------------------------------------
        private static ProductsByCategoryRequestModel GenerateProductSearchRequestFrom(
                                      JsonProductSearchRequest jsonProductSearchRequest)
        {
            ProductsByCategoryRequestModel productSearchRequest =
                                              new ProductsByCategoryRequestModel();

            productSearchRequest.NumberOfResultsPerPage = jsonProductSearchRequest.ItemsPerPage;
            productSearchRequest.Index = jsonProductSearchRequest.Index;
            productSearchRequest.CategoryId = jsonProductSearchRequest.CategoryId;
            productSearchRequest.SortBy = jsonProductSearchRequest.SortBy;


          

            if (jsonProductSearchRequest.RefinementGroups != null)
            {
                foreach (JsonRefinementGroup jsonRefinementGroup in
                                       jsonProductSearchRequest.RefinementGroups)
                {
                    switch ((RefinementGroupings)jsonRefinementGroup.GroupId)
                    {
                        case RefinementGroupings.brand:
                            productSearchRequest.BrandIds =
                                         jsonRefinementGroup.SelectedRefinements;
                            break;
                        case RefinementGroupings.color:
                            productSearchRequest.ColorIds =
                                         jsonRefinementGroup.SelectedRefinements;
                            break;
                        case RefinementGroupings.size:
                            productSearchRequest.SizeIds =
                                         jsonRefinementGroup.SelectedRefinements;
                            break;
                        default:
                            break;
                    }
                }
            }
            return productSearchRequest;
        }

        //---------------------------------------------------------------
        public ActionResult Details(int id, int forCatalog, Cart cart, string ReturnUrl)
        {
           
            ProductDetailViewModel productDetailView = new ProductDetailViewModel();
            
            productDetailView.Product = service.GetProduct(id);            
            productDetailView.Categories = base.GetCategories();
            productDetailView.Cart = cart;
            productDetailView.forCatalog = forCatalog;
            productDetailView.ReturnUrl = ReturnUrl;

            var colors = productDetailView.Product.ProductsQuantity.Where(p => p.Quantity > 0).
                                             Select(p => new { ColorId = p.ProductColorId })
                                             .Distinct();

            IEnumerable<ProductsQuantityViewModel> quantity = colors
                           .Select(p => new ProductsQuantityViewModel()
                           {
                               ColorId = p.ColorId,
                               SizesQuantity = productDetailView.Product.ProductsQuantity
                                                    .Where(s => p.ColorId == s.ProductColorId)
                                                    .OrderBy(s => s.ProductSizeId)
                                                    .Select(s => new SizeQuantityViewModel()
                                                    {
                                                        SizeId = s.ProductSizeId,
                                                        Quantity = s.Quantity
                                                    })
                           });

       

            productDetailView.ProductsQuantityViewModels = quantity;
            if (forCatalog == 1)
                return View(productDetailView);
            else
                return View("AdminDetails", productDetailView);
        }

        //----------------------------------------------------------------
        public FileContentResult GetImageAndSendBack2(HttpPostedFileBase f)
        {
            //for IE , can't do it at client site
            var ImageMimeType = f.ContentType;
            var ImageData = new byte[f.ContentLength];
            f.InputStream.Read(ImageData, 0, f.ContentLength);
            return File(ImageData, ImageMimeType);
        }


      //  [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult GetImageAndSendBack1(HttpPostedFileBase f)
        {
            //for IE , can't do it at client site
            var ImageMimeType = f.ContentType;
            var ImageData = new byte[f.ContentLength];
            f.InputStream.Read(ImageData, 0, f.ContentLength);
            return  Json(ImageData);
        }


//----------------------------------------------------------------------------
        private string GetUrlFileName(string url)
        {
            var parts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var last = parts[parts.Length - 1];
            return Path.GetFileNameWithoutExtension(last);
        }

        


        //----------------------------------------------------------------
        [HttpPost] 
        public ActionResult PreviewImage(HttpPostedFileBase file)
        {
            // Old IE, image for IFrame
            ViewBag.Message = "";
            ViewBag.Alt = GetUrlFileName(file.FileName);
            if (file.ContentLength > 3000000) ViewBag.ImgError = "File  size  more than 3000000. Image can't be added.";
            else
            {
                var inputByteArray = new byte[file.ContentLength];


                file.InputStream.Read(inputByteArray, 0, inputByteArray.Length);
                ViewBag.Mime = file.ContentType;


                byte[] outputBytes;
               
                using (var inputStream = new MemoryStream(inputByteArray))
                {
                    Image image = Image.FromStream(inputStream);
                    int hh = 198;
                    int ww = (int)((hh * image.Width) / image.Height);
                    Image thumbNail = image.GetThumbnailImage(ww, hh, null, IntPtr.Zero);

                    using (var outputStream = new MemoryStream())
                    {
                        thumbNail.Save(outputStream, image.RawFormat as ImageFormat);
                        outputBytes = outputStream.ToArray();
                    }

                     

                    //  IE8 only supports Base64 URI images up to 32kb in size
                   
                    if (outputBytes.Length < 32000) 
                        ViewBag.Message = Convert.ToBase64String(outputBytes, Base64FormattingOptions.InsertLineBreaks);

                }

            }

            return PartialView();
        }


        

        //--------------------------------------------------------------------
          public FileContentResult GetImage(int id)
          {
              ProductTitleImage f = ((ProductCatalogServices) service). GetProductTitleMainImage(id);
              if (f == null) return null;
              else  return File(f.ImageData, f.ImageMimeType);
          }

        //---------------------------------------------------------------------
          public FileContentResult GetProductImageFromSet(int imageId)
          {

              ProductTitleImage f = ((ProductCatalogServices)service).GetProductTitleImage(imageId);
              if (f == null) return null;
              return File(f.ImageData, f.ImageMimeType);

          }
    }
}
