﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using DDD.Domain.User;
using DDD.Service.Admin;
using DDD.Service.User;


namespace DDD.WebUI.Controllers
{
    public class UserController : Controller
    {
        private IAdminServices<UserProfile, int> userServices;


        public UserController(IAdminServices<UserProfile, int> userServices)
        {
            this.userServices = userServices;
        }  
        
        public ActionResult Index(int id = 5)
        {
            UserProfile user = userServices.GetBy(id); //new UserProfile();
            return View(user);
        }

        public ActionResult Anketa(int id)
        {
            UserProfile user = userServices.GetBy(id);
            return View(user.Anketa);
        }
       
        public ActionResult Search1(int Id=5)
        {
          
           UserProfile user = userServices.GetBy(Id);

           return View(user);
           
        }

         

        


        public ActionResult GetResult(int Id = 5)
        {
           
           UserProfile user = userServices.GetBy(Id);

           return View(user);
           
        }




        public ActionResult List(int Id)
        {
           // IEnumerable<UserProfile> users = userServices.GetAll();
            IEnumerable<UserProfile> users = ((UserServices)userServices).FindMatch(Id);
            return PartialView(users);
        }

        public ActionResult Details(int id)
        {
            UserProfile user = userServices.GetBy(id);
            return PartialView(user);
        }


        public ActionResult Info(int id)
        {
            UserProfile user = userServices.GetBy(id);
            return View(user);
        }

        public FileContentResult GetImage(int id)
        {

            UserProfile f = userServices.GetBy(id); //(UserServices)userServices.GetImage(id);
            if (f == null || f.ImageData ==null) return null;
            return File(f.ImageData, f.ImageMimeType);

        }

        public ActionResult Save(UserProfile user)
        {
            userServices.Save(user);
           
            return View("Index");
        }
    }
}
