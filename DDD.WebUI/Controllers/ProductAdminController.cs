﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Entities;
using DDD.Service.Admin;
using DDD.WebUI.ViewModels.Admin;
using DDD.Service.Model;
using System.IO;
using DDD.WebUI.Infrastructure.ValidationHelpers;
using DDD.Service.Product;



namespace DDD.WebUI.Controllers
{
    public class ProductAdminController : Controller
    {
        private IAdminServices<ProductTitle, int> adminServices;


        public ProductAdminController(IAdminServices<ProductTitle, int> adminServices)
        {
            this.adminServices = adminServices;
        }

        
        //--------------------------------------------------
        public ActionResult Index(int BrandId = 0, int CategoryId = 0,
           string OrderBy ="Id", int ItemsPerPage = 10, int CurrentPage = 1)
        {
            AllProductAttributeResponceModel attr = ((AdminProductServices)adminServices).GetAllProductAttributes(0);
            AdminHomePageViewModel adminHomePageViewModel = new AdminHomePageViewModel();


            adminHomePageViewModel.NumberOfResultsPerPage = ItemsPerPage;
            adminHomePageViewModel.Brands = attr.Brands.OrderBy(p => p.Name);
            adminHomePageViewModel.Categories = attr.Categories.OrderBy(p => p.Name);
            adminHomePageViewModel.Products = ((AdminProductServices)adminServices).GetAllBy(BrandId, CategoryId);
            adminHomePageViewModel.CurrentPage = CurrentPage;
            adminHomePageViewModel.AllItemsFound = adminHomePageViewModel.Products.Count();

            adminHomePageViewModel.OrderBy = OrderBy;
            var propertyInfo = typeof(ProductTitle).GetProperty(OrderBy);
            adminHomePageViewModel.Products=adminHomePageViewModel.
                      Products.OrderBy(p => propertyInfo.GetValue(p, null));

            adminHomePageViewModel.TotalPages = 
                       (int)Math.Ceiling((decimal)adminHomePageViewModel.AllItemsFound / ItemsPerPage);

            adminHomePageViewModel.Products = adminHomePageViewModel.Products                               
                               .Skip((CurrentPage -1) * ItemsPerPage).Take(ItemsPerPage);
                               
            return View(adminHomePageViewModel);

        }

        
        
        
        //-------------------------------------------------------------
        public ViewResult Create(string returnUrl)
        {
            AllProductAttributeResponceModel attr = ((AdminProductServices)adminServices).GetAllProductAttributes(0);

            ProductEditAdminViewModel productEditAdminViewModel = new ProductEditAdminViewModel();
            productEditAdminViewModel.ProductTitle = new ProductTitle()
            {
                Id = 0,
                Brand = new Brand() { Id = 0 },
                Category = new Category() { Id = 0 }
            };          
            productEditAdminViewModel.Brands = attr.Brands.OrderBy(p => p.Name);
            productEditAdminViewModel.Categories = attr.Categories.OrderBy(p => p.Name);
            productEditAdminViewModel.ReturnUrl = returnUrl;
         
            return View("Edit", productEditAdminViewModel);
        }

        
        //---------------------------------------------------------
        public ActionResult Edit(int id, string returnUrl)
        {
                     
            AllProductAttributeResponceModel attr = ((AdminProductServices)adminServices).GetAllProductAttributes(id);
            ProductEditAdminViewModel productEditAdminViewModel = new ProductEditAdminViewModel();

            productEditAdminViewModel.ProductTitle = adminServices.GetBy(id);
            productEditAdminViewModel.AddProductsQuantityToArray();                   
            productEditAdminViewModel.Brands = attr.Brands.OrderBy(p => p.Name);
            productEditAdminViewModel.Categories = attr.Categories.OrderBy(p => p.Name);
            productEditAdminViewModel.ReturnUrl = returnUrl;
            return View(productEditAdminViewModel);
        }

      
      
        //--------------------------------------------------------
        [HttpPost]
        public ActionResult Delete(int Id, string returnUrl)
        {
            ProductTitle productTitle = adminServices.GetBy(Id);            
            adminServices.Remove(Id);
            return Redirect(returnUrl);
        }



        //--------------------------------------------------
        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult DeleteAjax (  int Id)
        {
            ProductTitle productTitle = adminServices.GetBy(Id);            
            adminServices.Remove(Id);            
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        
      
        //--------------------------------------------------------------------------------
        [HttpPost]
        public ActionResult Edit(ProductTitle ProductTitle, IEnumerable<ProductQuantity> quantity,
                              string  returnUrl,
                              IEnumerable<HttpPostedFileBase> file, int[] fileId, int hasImages)
        {

            ModelState.CleanSometing("ProductTitle.Brand.Name", 
                        "ProductTitle.Brand.Description", "ProductTitle.Category.Name");
                          
            if (ProductTitle.Brand == null || ProductTitle.Brand.Id ==0)
            {               
                ModelState.AddModelError("ProductTitle.Brand.Id", "Please specify a  brand");
                ProductTitle.Brand.Id = 0;

            }
            if (ProductTitle.Category == null || ProductTitle.Category.Id == 0)
            {                
                ModelState.AddModelError("ProductTitle.Category.Id", "Please specify a category ");               
                ProductTitle.Category.Id = 0;
            }
           
     
            if (ModelState.IsValid)           
            {
                //Product Quantity Set
                ProductTitle.ProductsQuantity = ProductTitle.ProductsQuantity //quantity
                    .Where(p => p.Quantity > 0 || p.Id > 0)
                    .ToList();

                //Product Image Set
                if (hasImages == 1)
                {
                    var imageFiles = file.Select((image, i) => new
                    {

                        ImageOrderId = i + 1,
                        Image = image,
                        ProductTitleImage = (image == null ? null : new ProductTitleImage()
                        {
                            Name = "",
                            ImageOrderId = i + 1,
                            ImageMimeType = image.ContentType,
                            ImageData = new byte[image.ContentLength]
                        })
                    }).ToList();

                    foreach (var item in imageFiles)
                    {
                        if (item.Image != null)
                            item.Image.InputStream.Read(item.ProductTitleImage.ImageData, 0, item.Image.ContentLength);
                    }

                    var imageIds = fileId.Select((p, i) => new { ImageOrderId = i + 1, ImageId = p });

                    ProductTitle.ProductTitleImages = imageFiles
                                    .Join(imageIds,
                                     f => f.ImageOrderId,
                                     fId => fId.ImageOrderId,
                                     (f, fId) =>
                                         new ProductTitleImage()
                                         {
                                             Id = fId.ImageId,
                                             ImageOrderId = f.ImageOrderId,
                                             ImageData = (f.Image == null ? null : f.ProductTitleImage.ImageData),
                                             ImageMimeType = (f.Image == null ? "" : f.ProductTitleImage.ImageMimeType)
                                         }).ToList();
                }
                else
                {
                    ProductTitle.ProductTitleImages = null;
                }                 

                               
                adminServices.Save(ProductTitle);

                return Redirect(returnUrl);
            }
            else
            {
                ProductEditAdminViewModel productEditAdminViewModel = new ProductEditAdminViewModel();
                AllProductAttributeResponceModel attr =
                           ((AdminProductServices)adminServices).GetAllProductAttributes(ProductTitle.Id);
                productEditAdminViewModel.Brands = attr.Brands;
                productEditAdminViewModel.Categories = attr.Categories;               
                productEditAdminViewModel.ProductTitle = ProductTitle;
                productEditAdminViewModel.ReturnUrl = returnUrl;
                if (hasImages == 1 && fileId != null)
                {
                    productEditAdminViewModel.ProductTitle.ProductTitleImages = fileId
                        .Where(p => p > 0)
                        .Select(s => new ProductTitleImage() { Id = s })
                        .ToList();
                }
                 

                return View(productEditAdminViewModel);
            }
        }

    }
}
