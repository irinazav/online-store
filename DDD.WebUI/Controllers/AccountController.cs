﻿using System.Web.Mvc;
using DDD.WebUI.Infrastructure.Abstract;
using DDD.WebUI.ViewModels;
namespace DDD.WebUI.Controllers
{
    public class AccountController : Controller
    {
        IAuthProvider authProvider;
        public AccountController(IAuthProvider auth)
        {
            authProvider = auth;
        }


        public ViewResult UserProfile()
        {
            return View();
        }

       
        public ViewResult Create()
        {
            return View("CreateUserAccount");
        }


       // [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (authProvider.Authenticate(model.UserName, model.Password))
                {
                     return Redirect(returnUrl ?? Url.Action("Index", "Admin"));
                 }
                 else
                {
                    return View();
                }
            }
            else
            {
                    return View();
            }
        }
    }
}