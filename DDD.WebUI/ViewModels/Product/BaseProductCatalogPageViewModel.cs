﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Entities;
using DDD.Domain.Basket;
using DDD.Service.Product;

namespace DDD.WebUI.ViewModels.Product 
{
    public class BaseProductCatalogPageViewModel 
    {
                public IEnumerable<Category> Categories { get; set; }
                public Cart Cart { get; set; }
                public string ReturnUrl { get; set; }
                public PagingInfoModel PagingInfoModel { get; set; }
                public int ItemsPerPage { get; set; }
                public int SortBy { get; set; }
    }
}