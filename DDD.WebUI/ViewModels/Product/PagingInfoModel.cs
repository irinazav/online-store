﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.WebUI.ViewModels.Product
{
    public class PagingInfoModel
    {
        public int NumberOfTitlesFound { get; set; }  
        public int TotalNumberOfPages { get; set; } 
        public int CurrentPage { get; set; }

    }
}