﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Service.Product;
using DDD.Domain.Entities;

namespace DDD.WebUI.ViewModels.Product
{
    public class ProductSearchResultViewModel : BaseProductCatalogPageViewModel
    {

        public ProductSearchResultViewModel()  
        {
            RefinementGroups = new List<RefinementGroup>();
        }

        public string SelectedCategoryName { get; set; }
        public int SelectedCategory { get; set; }
        public IEnumerable<RefinementGroup> RefinementGroups { get; set; } 
        public IEnumerable<ProductTitle> Products { get; set; }
    }
}