﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Entities;

namespace DDD.WebUI.ViewModels.Product
{
    public class ProductDetailViewModel : BaseProductCatalogPageViewModel
    {
        public ProductTitle Product { get; set; }
        public IEnumerable<ProductsQuantityViewModel> ProductsQuantityViewModels { get; set; }
        public int forCatalog { get; set; }
    }
}