﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Entities;


namespace DDD.WebUI.ViewModels.Product
{
    public class HomePageViewModel : BaseProductCatalogPageViewModel
    {
        public IEnumerable<ProductTitle> Products { get; set; }
        
    }
    

}