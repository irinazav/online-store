﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.WebUI.ViewModels.Admin 
{
    public class BaseAdminPageViewModel : BasePageViewModel
    {
                public IEnumerable<AdminMenu> menu { get; set; }
                public string ReturnUrl;
    }
} 