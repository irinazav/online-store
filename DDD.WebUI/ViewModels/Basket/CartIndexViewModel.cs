﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Domain.Basket;
using DDD.Domain.Entities;

namespace DDD.WebUI.ViewModels.Basket
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
        
    }
}