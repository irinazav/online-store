﻿using System;
using System.Text;
using System.Web.Mvc;
using System.Web;
using DDD.WebUI.ViewModels.Product;

namespace DDD.WebUI.Infrastructure.HTMLHelpers
{     
    // must to add <add namespace="ProjectMVC.WebUI.Infrastructure.HtmlHelpers.HtmlHelpers"/> 
    //Добавляем пространство имен вспомогательного метода HTML в файл  Views/Web.config
    public static class PagingHelpers
    {

        public static MvcHtmlString PageLinks(
        this HtmlHelper html,
        PagingInfoModel pagingInfo,
        Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalNumberOfPages; i++)
            {
                TagBuilder tag = new TagBuilder("a"); // Construct an <a> tag
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("selected");
                else
                    tag.AddCssClass("notselected");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        //===============================================
        public static MvcHtmlString BuildPageLinksFrom(this HtmlHelper html, int currentPage,
                                       int totalPages, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= totalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == currentPage)
                    tag.AddCssClass("selected");
                else
                    tag.AddCssClass("notselected");
                result.AppendLine(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        //===============================================
        public static string Resolve(string resource)
        {
            return string.Format("{0}://{1}{2}{3}",
                  HttpContext.Current.Request.Url.Scheme,
                  HttpContext.Current.Request.ServerVariables["HTTP_HOST"],
                  (HttpContext.Current.Request.ApplicationPath.Equals("/")) ?
                            string.Empty : HttpContext.Current.Request.ApplicationPath,
                  resource);
        }

        public static string Resolve(this HtmlHelper html, string resource)
        {
            return Resolve(resource);
        }
    }
}