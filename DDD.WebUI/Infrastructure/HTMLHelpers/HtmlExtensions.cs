﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text;
using System.Web.Routing;
using DDD.Domain.Entities;
using DDD.Domain.Abstract;
using System.Text.RegularExpressions;


namespace DDD.WebUI.Infrastructure.HTMLHelpers
{
    public static class HtmlExtensions
    {

        public static MvcHtmlString Button(this HtmlHelper helper,
                                           string innerHtml,
                                           IDictionary<string, object> htmlAttributes)
        {
            var builder = new TagBuilder("button");
            builder.InnerHtml = innerHtml;
            builder.MergeAttributes(htmlAttributes);
            return MvcHtmlString.Create(builder.ToString());
        }



        //---------------------------------------------------------
        public static MvcHtmlString EnumDropDownList<TEnum>(this HtmlHelper htmlHelper, 
                                string name, TEnum selectedValue)
        {  
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum))
                .Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                from value in values
                select new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = (value.Equals(selectedValue))
                };

            return htmlHelper.DropDownList(
                name,
                items
                );
        }



        //-----------------------------------------------------------
        public static MvcHtmlString EnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,
                                Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
        
     
        //-----------------------------------------------------------
        public static MvcHtmlString EnumCheckBoxFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,
                                Expression<Func<TModel, TEnum>> expression)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                   // Selected = value.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }

        //---------------------------------------------------------------------

        public static MvcHtmlString DropDownListForOrderBy(this HtmlHelper htmlHelper,
                             string name, string caption, Dictionary<string,string> attr, string selectedValue = "Id")
        {

            if (attr == null)
            {
                attr = new Dictionary<string,string>();
                attr.Add("Id", "Id");
                attr.Add("Name", "Name");
                attr.Add("Price", "Price");
                
            }
            IEnumerable<SelectListItem> items =
                from item in attr
                select new SelectListItem
                {
                    Text = caption+ item.Value,
                    Value = item.Key,
                    Selected = item.Key == selectedValue
                };

            return htmlHelper.DropDownList(
                name,
                items
                );
        }
        //-------------------------------------------------------------------
        //DropDownList For Items Per Page (index list)
        public static MvcHtmlString DropDownListForItemsPerPage(this HtmlHelper htmlHelper,
                               string name, string caption, int[] ar, int selectedValue = 100)
        {

            if (ar == null)  ar = new int[] {2,3,4,6,8,10,12,20,40,60,80,100};
            IEnumerable<SelectListItem> items =
                from item in ar
                select new SelectListItem
                {
                    Text = caption+ " " +item.ToString(),
                    Value = item.ToString(),
                    Selected = item == selectedValue
                };

            return htmlHelper.DropDownList(
                name,
                items
                );
        }
        //------------------------------------------------------------------
        //ProductAdmin
        //  @Html.ProductAttributeDropDownList(attr.Categories, "Category", Model.Category.Id) 


        public static MvcHtmlString ProductAttributeDropDownList(this HtmlHelper htmlHelper,
                                IEnumerable<IProductAttribute> list, 
                                string name, int selectedValue) 
        {
            

            IEnumerable<SelectListItem> items =
                from item in list
                select new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString(),
                    Selected = (item.Id.Equals(selectedValue))
                };

            return htmlHelper.DropDownList(
                name,
                items
                );
        }

        //-----------------------------------------------------------------------------------------------------

        ///ProductAdmin
        //  @Html.ProductAttributeDropDownListFor(model => model.Id, attr.Categories)

        public static MvcHtmlString ProductAttributeDropDownListFor<TModel, T> (this HtmlHelper<TModel> htmlHelper,
                                        Expression<Func<TModel, T>> expression,
                                              IEnumerable<IProductAttribute> values)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.Name,
                    Value = value.Id.ToString(),
                    Selected = value.Id.Equals(metadata.Model)
                });

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
        //-----------------------------------------------


        public static MvcHtmlString DropDownListForIAttributeSourceFor<TModel, T>(this HtmlHelper<TModel> htmlHelper,
                                         Expression<Func<TModel, T>> expression,
                                               IEnumerable<IProductAttribute> values,
                                                 string zeroValue) 
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.Name,
                    Value = value.Id.ToString(),
                    Selected = value.Id.Equals(metadata.Model)
                });

            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }
        //-----------------------------------------------



        //From Matching
        //---------------------------------------------------------
        public static MvcHtmlString DropDownListForIAttributeSource<T>(this HtmlHelper htmlHelper,
                                     string name, int selectedValue,
                                     IEnumerable<T> values,
                                     string zeroValue) where T : IAttribute
        {



            IEnumerable<SelectListItem> items =
               from value in values
               select new SelectListItem
               {
                   Text = value.Name.ToString(),
                   Value = value.Id.ToString(),
                   Selected = (value.Id == selectedValue)
               };

            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownList(
                name,
                items
                );
        }

        //---------------------------------------------------------
        public static MvcHtmlString DropDownListForIAttributeSourceObject<T>(this HtmlHelper htmlHelper,
                                     string name, T selectedObject,
                                     IEnumerable<T> values,
                                     string zeroValue) where T : IAttribute
        {
            int selectedValue = 0;
            if (selectedObject != null) selectedValue = selectedObject.Id;


            IEnumerable<SelectListItem> items =
               from value in values
               select new SelectListItem
               {
                   Text = value.Name.ToString(),
                   Value = value.Id.ToString(),
                   Selected = (value.Id == selectedValue)
               };

            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownList(
                name,
                items
                );
        }
        //------------------------------------------------------------------------------------------------------
        public static MvcHtmlString MatchEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper,

                                Expression<Func<TModel, TEnum>> expression, string zeroValue)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            IEnumerable<TEnum> values = Enum.GetValues(typeof(TEnum)).Cast<TEnum>();

            IEnumerable<SelectListItem> items =
                values.Select(value => new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                });


            if (zeroValue != null)
            {
                IEnumerable<SelectListItem> zeroItems =
                             from zero in values.Take(1)
                             select new SelectListItem() { Text = zeroValue, Value = "0", Selected = false };
                items = zeroItems.Union(items);
            }

            return htmlHelper.DropDownListFor(
                expression,
                items
                );
        }

        //---------------------------------------------------------

       public static MvcHtmlString QuantityGrid(this HtmlHelper h,
           ProductQuantity[,] list, string caption, ModelStateDictionary modelState)
        {
            bool errors = false;
            string gg, id, fieldName;
            TagBuilder table = new TagBuilder("table");
            if (caption != null)
            {
                TagBuilder tableCaption = new TagBuilder("caption");
                tableCaption.SetInnerText(caption);
                table.InnerHtml += tableCaption;
            }
            table.AddCssClass("tblColorSizeQuantity");
            TagBuilder tr = new TagBuilder("tr");

            int rows = list.GetLength(1);
            int cols = list.GetLength(0);

            //tr Color Header 
            TagBuilder td = new TagBuilder("td");
            //td.SetInnerText("Size\\Color");
            tr.InnerHtml += td.ToString();


            for (int i = 0; i < cols; i++)
            {
                var productColor = ((ProductColor)(i + 1)).ToString();
                td = new TagBuilder("td");


                if (productColor == "Multi")
                {
                    td.AddCssClass("GridTitleMultiColor");
                }
                else
                {
                    td.AddCssClass("GridTitleColor");
                    td.MergeAttribute("bgcolor", productColor);
                }
                // td.SetInnerText(((ProductColor)(i+1)).ToString()); //display ColorName
                tr.InnerHtml += td.ToString();
            }
            table.InnerHtml += tr.ToString();

            // new tr             

            for (int i = 0; i < rows; i++)
            {
                //header - td
                tr = new TagBuilder("tr");
                td = new TagBuilder("td");
                td.SetInnerText(((ProductSize)(i + 1)).ToString());
                tr.InnerHtml += td.ToString();

                // td - content
                for (int ii = 0; ii < cols; ii++)
                {
                    td = new TagBuilder("td");

                    int quantity = 0, quantityId = 0;                    
                    if (list[ii, i] != null) 
                    {
                              quantity =  list[ii, i].Quantity;
                              quantityId =  list[ii, i].Id;
                    }
                
                    fieldName = "ProductTitle.ProductsQuantity["+ ((i * cols) + ii).ToString() + "].Quantity";
                    if (modelState[fieldName] == null) gg = quantity.ToString();
                    else gg = modelState[fieldName].Value.AttemptedValue;

                    fieldName = "ProductTitle.ProductsQuantity["+ ((i * cols) + ii).ToString() + "].Id";
                    if (modelState[fieldName] == null) id = quantityId.ToString();                       
                    else    id = modelState[fieldName].Value.AttemptedValue;
                    
                    fieldName = "ProductTitle.ProductsQuantity[" + ((i * cols) + ii).ToString() + "].Id";
                    if (modelState[fieldName] == null)
                    {
                        id = modelState[fieldName] == null ? quantity.ToString() :
                                                    modelState[fieldName].Value.AttemptedValue;
                    }
                    bool isNumber = Regex.IsMatch(gg, @"^\d+$");
                    errors = errors || (!isNumber);
                    if (isNumber && Int32.Parse(gg) == 0) gg = "0";
                    string cssClass;


                    if (!isNumber) cssClass = " class='inputColorSizeWrongTbl' ";
                    else if (gg == "0") cssClass = " class='inputColorSizeTbl' ";
                    else cssClass = " class='inputColorSizeSelectedTbl' ";

                    
                   
                    td.InnerHtml += String.Format(
                                        "<input type='text' " + cssClass + " name='ProductTitle.ProductsQuantity[");


                    td.InnerHtml += String.Format(
                                       ((i * cols) + ii).ToString() + "].Quantity' value = '" + gg + "'>");

                    td.InnerHtml += String.Format(

                                    "<input type='hidden' class='inputColorSizeHiddenTbl' name='ProductTitle.ProductsQuantity[" +
                                        ((i * cols) + ii).ToString() +
                                       "].ProductSizeId' value = '" + (1 + i).ToString() + "'>");

                    td.InnerHtml += String.Format(

                                    "<input type='hidden' class='inputColorSizeHiddenTbl' name='ProductTitle.ProductsQuantity[" +
                                        ((i * cols) + ii).ToString() +
                                       "].Id' value = '" + id.ToString() + "'>");


                    td.InnerHtml += String.Format(
                                    "<input type='hidden' class='inputColorSizeHiddenTbl' name='ProductTitle.ProductsQuantity[" +
                                         ((i * cols) + ii).ToString() +
                                       "].ProductColorId' value = '" + (1 + ii).ToString() + "'>");

                    tr.InnerHtml += td.ToString();
                }
                table.InnerHtml += tr.ToString();
            }

             
             var div = new TagBuilder("div");          
             div.AddCssClass("divColorSizeQuantity");
             div.InnerHtml += table.ToString();

            if (errors)
            {
                string error = "<span class='field-validation-error' data-valmsg-for='ProductTitle.ProductsQuantity' data-valmsg-replace='true'>Please enter a valid quantity</span>";
                return new MvcHtmlString(error + div.ToString());
            }
            else return new MvcHtmlString(div.ToString());
                
                
           
        }
        //-------------------------------------------------

      
           public static MvcHtmlString Hyperlink(this HtmlHelper helper, string url, string linkText)
           {
               return MvcHtmlString.Create(String.Format("<a href='{0}'>{1}</a>", url, linkText));
           }

        //-------------------------------------------------

        

    }
}