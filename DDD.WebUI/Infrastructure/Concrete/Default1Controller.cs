﻿using System.Web.Security;
using DDD.WebUI.Infrastructure.Abstract;
namespace DDD.WebUI.Infrastructure.Concrete
{
    public class FormsAuthProvider : IAuthProvider
    {
        //Последним шагом будет регистрация FormsAuthProvider в
        //методе AddBindings класса NinjectControllerFactory


        public bool Authenticate(string username, string password)
        {
            bool result = FormsAuthentication.Authenticate(username, password);
            if (result)
            {
                FormsAuthentication.SetAuthCookie(username, false);
            }
            return result;
        }
    }
}
