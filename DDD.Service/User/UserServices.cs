﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Repositories.Abstract;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Service.Admin;
using DDD.Domain.User;
using DDD.Repositories.EF;


namespace DDD.Service.User
{
    public class UserServices : IAdminServices<UserProfile, int>
    {

        private readonly IRepositoryCRUD<UserProfile, int> _repository;

        public UserServices(IRepositoryCRUD<UserProfile, int> repository)
        {

            _repository = repository;
        }

        public UserProfile GetBy(int Id)
        {
            return _repository.FindBy(Id);
        }


        public IEnumerable<UserProfile> GetAll()
        {
            return _repository.FindAll();
        }

        public void Save(UserProfile entity) { _repository.Save(entity); }
        public void Add(UserProfile entity) { _repository.Add(entity); }
        public void Remove(int id) { _repository.Remove(id); }

        public UserProfile GetImage(int id)
        {
            return ((UserRepository)_repository).GetImage(id);

        }

        public IEnumerable<UserProfile> FindMatch(int id)
        {
            return ((UserRepository)_repository).FindMatch(id);
        }

    }

}

