﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Service.Model;
using DDD.Service.Product;
using DDD.Domain.Entities;

namespace DDD.Service.Model
{
    public class ProductsByCategoryResponseModel
    {
        public string SelectedCategoryName { get; set; }
        public int SelectedCategory { get; set; }

        public IEnumerable<RefinementGroup> RefinementGroups { get; set; }

        public int NumberOfTitlesFound { get; set; }
        public int TotalNumberOfPages { get; set; }
        public int CurrentPage { get; set; }
        public IEnumerable<ProductTitle> Products { get; set; }

    }
}