﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Entities;

namespace DDD.Service.Model
{
    public class AllProductAttributeResponceModel
    {

        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        
    }
}