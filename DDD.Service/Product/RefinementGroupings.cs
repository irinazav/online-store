﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.Service.Product
{
    public enum RefinementGroupings
    {
        brand = 1,
        size = 2,
        color = 3
    }
}