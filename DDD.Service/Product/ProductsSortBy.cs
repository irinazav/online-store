﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DDD.Service.Product
{
    public enum ProductsSortBy
    {
        PriceHighToLow = 1,
        PriceLowToHigh = 2
    }
}