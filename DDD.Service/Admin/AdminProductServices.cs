﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Repositories.Abstract;
using DDD.Repositories.EF;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.Service.Model;
using System.IO;
using System.Web.Mvc;



namespace DDD.Service.Admin
{
    public class AdminProductServices : IAdminServices<ProductTitle, int>
    {

        private readonly IRepositoryCRUD<ProductTitle, int> _repository;
        //-----------------------------------------------------------
        public AdminProductServices(IRepositoryCRUD<ProductTitle, int> repository)
                                      
        {
           
            _repository = repository;
        }
        //------------------------------------------------
        public ProductTitle GetBy(int Id)
        {
            return _repository.FindBy(Id);
        }
       
        //---------------------------------------------------
        public IEnumerable<ProductTitle> GetAll()
        {
           return  _repository.FindAll();
        }



        public IEnumerable<ProductTitle> GetAllBy(int BrandId, int CategoryId)
        {
            return ((ProductTitleRepository)_repository).FindAllBy(BrandId, CategoryId);
        }

        //--------------------------------------------------
        public IEnumerable<ProductTitle> By(int BrandId, int CategoryId)
        {
            return ((ProductTitleRepository)_repository).FindAllBy(BrandId, CategoryId);
        }
       //--------------------------------------------------------------
        public void Add(ProductTitle entity) { _repository.Add(entity); }

        //--------------------------------------------------------------
        public void Remove(int id) { _repository.Remove(id); }

        
        //----------------------------------------------------------------
       public  AllProductAttributeResponceModel GetAllProductAttributes(int id)
        {
           AllProductAttributeResponceModel responseModel = new AllProductAttributeResponceModel();

           ProductTitleRepository productTitleRepository = ((ProductTitleRepository)_repository); 

           responseModel.Brands = productTitleRepository.GetProductBrands();
           responseModel.Categories = productTitleRepository.GetProductCategories();
         
          return responseModel;
        }


        //------------------------------------------------------------------
       


       public void Save(ProductTitle entity) 
       { 
           _repository.Save(entity);       
       }

       public void Add(ProductTitle entity, ProductImage productImage)
       {
           
       }

    }
}

