﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Repositories.Abstract;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;


namespace DDD.Service.Admin
{
    public class AdminBrandServices : IAdminServices<Brand, int>
    {

        private readonly IRepositoryCRUD<Brand, int> _repository;

        public AdminBrandServices(IRepositoryCRUD<Brand, int> repository)
        {

            _repository = repository;
        }

        public Brand GetBy(int Id)
        {
            return _repository.FindBy(Id);
        }


        public IEnumerable<Brand> GetAll()
        {
            return _repository.FindAll();
        }

        public void Save(Brand entity) { _repository.Save(entity); }
        public void Add(Brand entity) { _repository.Add(entity); }
        public void Remove(int id) { _repository.Remove(id); }

    }



}

