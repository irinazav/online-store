﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DDD.Repositories.Abstract;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;


namespace DDD.Service.Admin
{
   public class AdminCategoryServices : IAdminServices<Category, int>
    {

        private readonly IRepositoryCRUD<Category, int> _categoryRepository;

        public AdminCategoryServices(IRepositoryCRUD<Category, int> categoryRepository)
                                      
        {
           
            _categoryRepository = categoryRepository;
        }

        public Category GetBy(int Id)
        {
            return _categoryRepository.FindBy(Id);
        }
        

        public IEnumerable<Category> GetAll()
        {
           return  _categoryRepository.FindAll();
        }

        public void Save(Category entity) { _categoryRepository.Save(entity); }
        public void Add(Category entity) { _categoryRepository.Add(entity); }
        public void Remove(int id) { _categoryRepository.Remove(id); }
        
    }



}

