﻿using NUnit.Framework;
using Moq;
using DDD.Domain.Abstract;
using DDD.Domain.Entities;
using DDD.WebUI.Controllers;
using DDD.WebUI.ViewModels;
using DDD.Repositories.EF;
using DDD.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DDD.WebUI.Infrastructure.HTMLHelpers;

namespace DDD.UnitTests
{
    [TestFixture]
    public class ProductTest1
    {
   [Test] 
        public void Can_Create_Products()
        {
          ProductTitle[] products = new ProductTitle[] {
            new ProductTitle {Id = 1, Name = "P1", Price =5.17m},
             new ProductTitle {Id = 2, Name = "P2", Price =5.17m},
                new ProductTitle {Id = 3, Name = "P3", Price =5.17m},
                    new ProductTitle {Id = 4, Name = "P4", Price =5.17m},
                         new ProductTitle {Id = 5, Name = "P5", Price =5.17m}};
    
           Mock<IRepositoryCRUD<ProductTitle, int>> mock = new Mock<IRepositoryCRUD<ProductTitle, int>>();
           mock.Setup(m => m. GetDBSet()).Returns(products.AsQueryable());
           mock.Setup(m => m.FindBy(It.IsAny<int>())).Returns(products.Where(p => p.Id == 1).FirstOrDefault());
          
           decimal total = products.Sum(p => p.Price );
           decimal totalByMock = mock.Object.GetDBSet().Sum(p => p.Price );
           Assert.AreEqual(total, totalByMock);
           Assert.AreEqual(mock.Object.FindBy(4), products.Where(p => p.Id == 1).FirstOrDefault());
           
        }
    }
}
  