﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace DDD.Domain.Abstract
{
    public interface IProductAttribute
    {   
        int Id { get; set; }
        string Name { get; set; }
    }
}
