﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DDD.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.IO;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;


namespace  DDD.Domain.User
{
    public enum Gender {  Women = 1, Men = 2 };
    

    public class UserProfile : EntityBase<int>, IAggregateRoot

    {
        [HiddenInput(DisplayValue = false)]
           public string Name { get; set; } 
          
         //  [Required(ErrorMessage = "Please enter your first name")]
          [DisplayName("First Name")]
           public string   FirstName {get; set;}

       // [Required(ErrorMessage = "Please enter your last name")]
         [DisplayName("Last Name")]
           public string   LastName  {get; set;}

     

     //    [Required(ErrorMessage = "Please enter your gender")]

         [DisplayName("Gender")]
        public int GenderId { get; set; }

        //How to use Enum in C# 
        //http://csharp.net-informations.com/statements/enum.htm

        [NotMapped]
        [DisplayName("Gender")]
        public Gender GenderValue
        {
            get{ return (Gender) GenderId; }
            set { GenderId = (int)value; }
            }

       // [DataType(DataType.Date)]
        [DisplayName("Birth Day")]
        public DateTime BirthDay { get; set; }

        [NotMapped]
        public int Age
        {    get
            {
                DateTime today = DateTime.Today;
                int age = today.Year - BirthDay.Year;
                if (BirthDay > today.AddYears(-age)) age--;
                return age;
            }
            set { 
                   BirthDay = DateTime.Now.AddYears(-value); 
                }
        }


     //   [Required(ErrorMessage = "Please enter your e-mail address")] 
          [DisplayName("EMail address")]
           public string  Email  {get; set;}

      
        

      //  [Required(ErrorMessage = "Please enter your password")]
   
        public string  Password  {get; set;}


       
   //    [Required(ErrorMessage = "Please enter your e-mail address")] 
        [DisplayName("Member since")]
       // [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

       public Anketa Anketa { get; set; }



[HiddenInput(DisplayValue = false)]
       public byte[] ImageData { get; set; }

        [HiddenInput(DisplayValue = false)]
       public string ImageMimeType { get; set; }

        public void ProductImage (HttpPostedFileBase image)
        {       
            Name = (new DateTime()).ToShortDateString();
            ImageMimeType = image.ContentType;
            ImageData = new byte[image.ContentLength];
            image.InputStream.Read(ImageData, 0, image.ContentLength);
        }



        public void SaveImageFromDisc(string filePath)
        {
            //open file from the disk (file path is the path to the file to be opened)
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                ImageData = new byte[fileStream.Length];
                fileStream.Read(ImageData, 0, (int)fileStream.Length);
                ImageMimeType = "image/jpeg";
            }
        }  
    }
}
