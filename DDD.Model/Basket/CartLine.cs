﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Entities;
using DDD.Domain.Abstract;

namespace DDD.Domain.Basket
{
    public class CartLine:  EntityBase<int>
    {
        public string Name { get; set; }
        public int ImageId { get; set; }
        public string BrandName { get; set; }
        public int ProductColorId { get; set; }
        public int ProductSizeId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

        public string ProductColorName
        {
            get { return ((ProductColor)ProductColorId).ToString(); }
        }

        public string ProductSizeName
        {
            get { return ((ProductSize)ProductSizeId).ToString(); }
        }

       

    }
}
