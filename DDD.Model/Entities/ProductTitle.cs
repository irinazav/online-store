﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;
using DDD.Domain.CustomAttribute;



namespace DDD.Domain.Entities
    {
    public class ProductTitle : EntityBase<int>, IAggregateRoot
        {
      
            [Required(ErrorMessage = "Please enter a name")]
            public string Name { get; set; }

            [Required(ErrorMessage = "Please enter a description")]
            [DataType(DataType.MultilineText)]
            [StringLength(1024)]
            public string Description{ get; set;}

            [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]            
            [Range(0.01, int.MaxValue, ErrorMessage = "Please enter price more than zero")]
            [Required(ErrorMessage = "Please enter a valid price")]
            public decimal Price { get; set; }

            [RequiredMustHaveId(id = 0, ErrorMessage = "Please specify a category")]
            public Category Category { get; set; }

            [RequiredMustHaveId(id = 0, ErrorMessage = "Please specify a brand")]
            public Brand Brand { get; set; }            
            
            [DisplayName("Quantity for color & size")]
            public virtual ICollection<ProductQuantity> ProductsQuantity { get; set; }

            [DisplayName("Image set")]   
            public virtual ICollection<ProductTitleImage> ProductTitleImages { get; set; }

            
        }

    }

