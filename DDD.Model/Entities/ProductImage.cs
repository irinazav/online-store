﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web;
using DDD.Domain.Abstract;
using System.IO;

namespace DDD.Domain.Entities
    
{
    public class ProductImage : EntityBase<int>, IAggregateRoot, IProductAttribute
    {
       
        public string Name { get; set; }
       
        public byte[] ImageData { get; set; }
        public string ImageMimeType { get; set; }
        public int ImageOrderId { get; set; }

        public ProductImage() { }
        public ProductImage (HttpPostedFileBase image)
        {               
            Name = (new DateTime()).ToShortDateString();
            ImageMimeType = image.ContentType;
            ImageData = new byte[image.ContentLength];
            image.InputStream.Read(ImageData, 0, image.ContentLength);
        }

        public void SaveImageFromDisc(string filePath)
        {
            //open file from the disk (file path is the path to the file to be opened)
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                ImageData = new byte[fileStream.Length];
                fileStream.Read(ImageData, 0, (int)fileStream.Length);
                ImageMimeType = "image/jpeg";
            }
        }  
       
    }
}
