﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDD.Domain.Entities
{

    public enum ProductColor
    {
        Blue = 1,
        Yellow = 2,
        Red = 3,
        Green = 4,
        Brown = 5,
        White = 6,
        Gray = 7,
        Black = 8,
        Multi = 9
    };
    
}
