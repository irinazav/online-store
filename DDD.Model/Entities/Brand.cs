﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DDD.Domain.Entities
{
    public class Brand : EntityBase<int>, IAggregateRoot, IProductAttribute, IAttribute
    {
        [Required(ErrorMessage = "Please enter a brand name")]
        [StringLength(1024)]
        [DisplayName("Brand")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Please enter a description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public virtual ICollection<ProductTitle> ProductTitles { get; set; }
       
    }
}
