﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;
using System.ComponentModel.DataAnnotations;

namespace DDD.Domain.Entities
{
    public class ProductQuantity : EntityBase<int>, IProductAttribute
    {

        public string Name { get; set; }

        //-----------------------------------
        public int ProductSizeId { get; set; }

        public ProductSize ProductSizeValue
        {
            get { return (ProductSize)ProductSizeId; }
            set { ProductSizeId = (int)value; }
        }

        //----------------------------------
        public int ProductColorId { get; set; }

        public ProductColor ProductColorValue
        {
            get { return (ProductColor)ProductColorId; }
            set { ProductColorId = (int)value; }
        }
        //-------------------------------------------------

        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter a valid quantity number")]
        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }


        public virtual ICollection<ProductTitle> ProductTitles { get; set; }
    
      
    }
}
