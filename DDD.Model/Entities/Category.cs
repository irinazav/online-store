﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDD.Domain.Abstract;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DDD.Domain.Entities
{
    public class Category : EntityBase<int>, IAggregateRoot, IProductAttribute, IAttribute
    {
        [Required(ErrorMessage = "Please enter a category name")]
        [DisplayName("Category")]
        public string Name { get; set; }
        public  ICollection<ProductTitle> ProductTitles { get; set; }
      
    }
}
