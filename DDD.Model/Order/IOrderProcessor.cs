﻿using DDD.Domain.Basket;
using DDD.Domain.Shipping;

namespace DDD.Domain.Order
{
    public interface IOrderProcessor
    {
        void ProcessOrder(Cart cart, ShippingDetails shippingDetails);
    }
}
