﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using DDD.Domain.Abstract;

namespace DDD.Domain.CustomAttribute
{
    public class RequiredMustHaveIdAttribute : RequiredAttribute 
    {
        public int id { get; set; }
       

        public override bool IsValid(object value)
        {
           
           return //base.IsValid(value) &&
                value is IAttribute &&
                ((IAttribute)value).Id > id;
        }
    }
}
